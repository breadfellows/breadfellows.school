#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Clare Breen'
SITENAME = 'breadfellows.school'
SITEURL = 'breadfellows.school'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Dublin'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME_STATIC_DIR = 'theme'
THEME = 'theme/breadfellows'
CSS_FILE = 'screen.css'
STATIC_PATHS = [
    'images',
    'Living\ Arts\ Project/images',
    'Documents',
]
# URL settings
ARTICLE_URL = '{category}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{slug}.html'
PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'
CATEGORY_URL = '{slug}.html'
CATEGORY_SAVE_AS = '{slug}.html'
ARTICLE_LANG_URL = '{category}/{slug}-{lang}.html'
ARTICLE_LANG_SAVE_AS = '{category}/{slug}-{lang}.html'
PAGE_LANG_URL = '{slug}-{lang}.html'
PAGE_LANG_SAVE_AS = '{slug}-{lang}.html'
DRAFT_URL = '{slug}.html'
DRAFT_SAVE_AS = '{slug}.html'
TAG_SAVE_AS = 'tag/{slug}.html'

DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = False

MENU_ITEMS = (
    ('about', '#'),
    ('methodology', '#'),
)


USE_FOLDER_AS_CATEGORY = True

# INDEX_SAVE_AS = 'full_index.html'

DEFAULT_PAGINATION = 999

PLUGINS = [
    #'compass_process',
    'minchin.pelican.plugins.image_process',
]

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

IMAGE_PROCESS = {
    'thumbnails': ["scale_in 200 9999 True"],
    'halfwidth': ["scale_in 640 480 True"],
    'fullwidth': ["scale_in 900 900 True"],
}
