// function moveDiv() {
  // $(".bubble").each(function () {
    // var maxLeft = window.innerWidth - 400;
    // var maxTop = window.innerHeight - 400;
    // var leftPos = Math.floor(Math.random() * (maxLeft + 1))
    // var topPos = Math.floor(Math.random() * (maxTop + 1))
    // $(this).css({ left: leftPos, top: topPos }).fadeIn(1000);
  // });
// };
//
// moveDiv();
//setInterval(moveDiv, 1000);

// jQuery.fn.verticalMarquee = function(vertSpeed, horiSpeed, index) {
//
//     this.css('float', 'left');
//
//     vertSpeed = vertSpeed || 1;
//     horiSpeed = 1/horiSpeed || 1;
//
//     var windowH = this.parent().height(),
//         thisH = this.height(),
//         parentW = (this.parent().width() - this.width()) / 2,
//         rand = Math.random() * (index * 1000),
//         current = this;
//
//     this.css('margin-top', windowH + thisH);
//     this.parent().css('overflow', 'hidden');
//
//     setInterval(function() {
//         current.css({
//             marginTop: function(n, v) {
//                 return parseFloat(v) - vertSpeed;
//             },
//             marginLeft: function(n, v) {
//                 return (Math.sin(new Date().getTime() / (horiSpeed * 1000) + rand) + 1) * parentW;
//             }
//         });
//     }, 15);
//
//     setInterval(function() {
//         if (parseFloat(current.css('margin-top')) < -thisH) {
//             current.css('margin-top', windowH + thisH);
//         }
//     }, 250);
// };
// var message = 1;
// $('.bubble').each(function(message) {
//     $(this).verticalMarquee(0.4, 0.2, message);
//     message++
// });

document.addEventListener("DOMContentLoaded", function() {
  if ($(window).width() > 600) {
      // console.log("window is more than 600px wide")
      var leftHeight =  $(".half-left").height();
      var rightHeight = $(".half-right").height();
      if (rightHeight > leftHeight){
        $(".half-left").height(rightHeight);
      }
      else{
        $(".half-right").height(leftHeight);
      }
  }

  $(".reveal-details").click(function () {
      $('.collapse').slideToggle("slide");
    });
  $("#about-button").click(function () {
      $("#about-modal").toggle();
      $('#modal_drop-in').toggle();
      if($('#methodology-modal').is(':visible'))
      {
        $("#methodology-modal").toggle();
        $("#modal_drop-in2").toggle();
      }
  });
  $("#methodology-button").click(function () {
      $("#methodology-modal").toggle();
      $('#modal_drop-in2').toggle();

      if($('#about-modal').is(':visible'))
      {
        $("#about-modal").toggle();
        $("#modal_drop-in").toggle();
      }
  });

});
