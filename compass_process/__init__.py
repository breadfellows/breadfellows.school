import shlex
import subprocess

from pelican import signals, contents
from pelican.readers import METADATA_PROCESSORS
from pelican.utils import get_date

def process_css(pelican):
	path = '/home/colm/git/breadfellows.school/theme/breadfellows/static/sassaparilla/compass'
	cmd = 'compass compile ' + path
	print(cmd)
	print("calling compass from plugin")
	call_params = shlex.split(cmd)
	subprocess.call(call_params)

def register():
    signals.initialized.connect(process_css)
