Title: Documenta14
Date: 2017/04/01
rating: 4
featured_image: images/documenta14/documenta7.jpg
Tags: galleries, schools,

# What shifts? What drifts? What remains?
were three questions that created the climate of inquiry for documenta 14’s aneducation.

![](/images/documenta14/documenta5.jpg)

In April 2017 I moved to Athens to work as part of the documenta14 education team as a member of the chorus. The program, developed relationships with learning institutions, artist-run spaces, and neighborhoods to investigate the correlation between art, education, and the aesthetics of human togetherness.

![](/images/documenta14/Chorus_athens.jpg)

![](/images/documenta14/documenta2.jpg){.third-centered}
![](/images/documenta14/documenta1.jpg){.third-centered}
![](/images/documenta14/documenta3.jpg){.third-centered}
{.grid}

Traditional art education and mediation relies on the basis of a script; aneducation's methodology relied on the competencies and biographies of documenta 14 chorus members to facilitate spaces for dialogue and exchange, responding to the exhibitions together. The chorus was made up of artists, researchers, archeologists, anthropologists, musicians and activists and rather than "tours" we developed "walks" that were as dependent on our own perspectives as the public who came on walks with us.

I gave walks and workshops for adults and children across the city, aneducation was a daily practice and a public learning. It addresses education as an open form, referring to the methodology of Oskar and Zofia Hansen, and as a space for spontaneous gestures and occurrences that “will awaken the desire of existence.” We gave walks through rather than a walks around the exhbitions; learning with and through the artworks.

![](/images/documenta14/documenta10.jpg){.third-centered}
![](/images/documenta14/documenta8.jpg){.third-centered}
![](/images/documenta14/documenta11.jpg){.third-centered}
{.grid}