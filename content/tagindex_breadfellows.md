Title: breadfellows Chats
Date: 2020/08
rating: 1
save_as: breadfellows_index.html
featured_image: images/breadfellows/1.jpg
Template: tag_index
Status: draft

# Breadfellows' Chats

Breadfellows' Chats is an artistic research methodology I have developed that adapts to many different environments. It is focused on collaborative making in informal learning spaces and is process oriented.

During a Breadfellows' Chat two or more people come together to have a conversation and to make an object together from clay. I use clay as a medium to extend dialogue. It stimulates verbal exchange and adds a physical aspect to a conversation. I call these clay objects companions.

The word companion means friend - someone or something you spend time with. “Companion” is derived from the Latin “com” meaning with and the French “panis” meaning bread, so companion literally means “with bread” or sharing bread together, hence the name Breadfellows' Chats.

It should always be possible to use the companion to share a meal. The conversation that takes place during a Breadfellows' Chat is as important as the companions themselves. Breadfellows' Chats foreground conversation and collaborative making as co-constructive learning methodologies. I aim to create spaces in which non-hierarchical learning takes place through artistic practice between the public, artists and institutions.

<!-- below will be an index of all the articles tagged with the same tag as this pages title -->
