Title: communities
Date: 2020/08
rating: 1
save_as: communities_index.html
featured_image: images/take_a_part/takeapart8.jpg
Template: tag_index
Status: draft

# Working with communities

I have developed various projects in partnership with community groups. Below are some recent projects funded by the Arts Council of Ireland, Creative Ireland and Carlow Arts Office.

<!-- below will be an index of all the articles tagged with the same tag as this pages title -->
