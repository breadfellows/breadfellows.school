Title: THRIECE Research Partnership
Date: 2019/02/02
rating: 3
featured_image: images/thriece/thriece01.jpg
Tags: schools, resources, publications, breadfellows, research

# Working with THRIECE Research Partnership

![](/images/thriece/thriece01.jpg)

I was an associate artist to the THRIECE Partnership (Teaching for Holistic, Relational and Inclusive Early Childhood Education), an Erasmus+ research project in Early Childhood Education and Care.
Beginning in December 2017, THRIECE was a response to the current shift in European education towards neoliberal discourses that prioritise ‘valuable’ knowledge while devaluing areas not seen to have utility in a global economy. This relentless drive for ‘quality’ is resulting in the narrowing of curricula, the standardisation of achievement through testing and international ranking, and in a fixation on quantitative measurement as the true arbiter of quality education.
I was involved as an associate artist in this project. I coordinated Breadfellows' Chats workshops with participating teachers, researchers and children, I also presented my practice and research at some of the gatherings and exhibited ceramics made during these workshops at the culminating conference in VISUAL, Carlow.

The THRIECE partnership believes that this current situation is insufficient if contemporary Europe is to effectively address social inclusion. Early Childhood Education and Care (ECEC) is widely promoted as a means to achieving inclusion for the 508.5 million young people in Europe. THRIECE addressed the worrying focus on bringing settings and children to predetermined fixed points, irrespective of background or culture. Such approaches may propagate deficit models, with some children and communities seen as insufficient based on social class, language or ethnicity. If treated uncritically, measures of ‘quality’ can become instruments of social exclusion.
The project took place in Ireland, Poland and Portugal and comprised three pre-schools, three primary schools and four Higher institutes of Educations. THRIECE proposed an alternative view of quality in ECEC that supports inclusion through recognition of the crucial nature of relationships and interactions. As part of this project, a new module for Early Childhood Education in Ireland, Poland and Portugal was produced.
