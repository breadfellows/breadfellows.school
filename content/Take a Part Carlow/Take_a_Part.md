Title: Take a Part Carlow
Date: 2020/01/01
rating: 3
featured_image: images/take_a_part/takeapart8.jpg
Tags: communities, breadfellows, publications, galleries

# Take a Part Carlow – Breadfellows' Chats

May 2019 – January 2020
Take a Part Carlow is a participatory community art initiative working in the Tullow Road area of Carlow Town. They aim to make art in their local area, to include, empower and support the Tullow Road communities to build artistic capacity to bring communities together. Take A Part Carlow aims to give a sense of place, belonging and local ownership and direct decision making from the ground up.

![](/images/take_a_part/takeapart7.png)
![](/images/take_a_part/takeapart4.png)

I was invited by the group to work through Breadfellows' Chats to engage in dialogue about their projects to date, to develop ceramics together and to produce a public event and publication about their processes and future plans for the project.

![](/images/take_a_part/takeapart1.png){.third-centered}
![](/images/take_a_part/takeapart3.png){.third-centered}
![](/images/take_a_part/takeapart6.png){.third-centered}
{.grid}

Each week, a member of the group volunteered their kitchen to be used as a space to make ceramics together, during these sessions we talked about the hopes for future projects and what they had achieved so far. These ceramics were then glazed by the group and used to have a big community meal and celebration at An Gairdín Beo, a community garden in Carlow.
