Title: Breadfellows' Chats
Date: 2020/08/08
rating: 5
featured_image: images/breadfellows/1.jpg
Tags: breadfellows, galleries, schools, resources, publications, communities, research

# Breadfellows' Chats - An Embodied Research Methodology

Breadfellows' Chats is an artistic research methodology I have developed that adapts to many different environments. It is focused on collaborative making in informal learning spaces and is process oriented.

During a Breadfellows' Chat two or more people come together to have a conversation and to make an object together from clay. I use clay as a medium to extend dialogue. It stimulates verbal exchange and adds a physical aspect to a conversation. I call these clay objects companions.

The word companion means friend - someone or something you spend time with. “Companion” is derived from the Latin “com” meaning with and the French “panis” meaning bread, so companion literally means “with bread” or sharing bread together, hence the name Breadfellows' Chats.

It should always be possible to use the companion to share a meal. The conversation that takes place during a Breadfellows' Chat is as important as the companions themselves. Breadfellows' Chats foreground conversation and collaborative making as co-constructive learning methodologies. I aim to create spaces in which non-hierarchical learning takes place through artistic practice between the public, artists and institutions.

![](/images/breadfellows/1.jpg)

![](/images/breadfellows/1.jpg)

![](/images/breadfellows/6.PNG)

![](/images/breadfellows/3.jpg)
![](/images/breadfellows/2.JPG)
![](/images/breadfellows/5.JPG)

![](/images/breadfellows/7.jpg)
![](/images/breadfellows/13.jpg)
![](/images/breadfellows/17.JPG)

<iframe src="https://player.vimeo.com/video/290640691" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/290640691">Breadfellows&#039; Chats</a> from <a href="https://vimeo.com/aieportal">Arts in Education Portal</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
