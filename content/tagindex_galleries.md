Title: galleries
Date: 2020/08
rating: 1
save_as: galleries_index.html
featured_image: images/visual_workshops/schools27.jpg
Template: tag_index
Status: draft

# Working with Galleries

In my practice as a learning curator and artist I have worked in many contexts developing workshops responding to exhibitions, learning resources and interactive installations.

<!-- below will be an index of all the articles tagged with the same tag as this pages title -->
