Title: Material Conversations
Date: 2018/01/09
rating: 5
Featured_image: /images/material_conversations/mc1.jpg
Homepage_summary: Material Conversations was an interactive installation responding to Eva Rothchild's exhibition The Shrinking Universe at VISUAL.

# Material Conversations
Material Conversations was an interactive installation responding to Eva Rothchild's exhibition The Shrinking Universe at VISUAL. This installation offered a space to play with materials, think about light and form and to construct sculptures using many of the materials Eva Rothchild uses in her own work.

![](/images/material_conversations/mc1.jpg)

The text in the space read: "Feel free to arrange, stack, heap, fill, gather, empty, draw, spread, build and imagine possibilities for these materials - make your own sculptures."

![](/images/material_conversations/mc2.jpg){.third-centered}
![](/images/material_conversations/mc3.jpg){.third-centered}
![](/images/material_conversations/mc5.jpg){.third-centered}
{.grid}

To perceive means to become aware of something through the senses; looking, smelling, tasting, hearing and touching. Our brains organise this information so we can make sense of the world. Lisa la Feuvre calls our bodies "perceiving machines". Sculpture is part of the world we live in and it is a language. Some of the things this language is concerned with are form, balance, material, weight, surface, finish, volume, scale, shape, gravity, figuration and abstraction. Playing with this language is what sculpture is all about.
As the exhibition went on, the space changed and expanded.

![](/images/material_conversations/mc6.jpg)
![](/images/material_conversations/VIS0220ER097.jpg)

For 5 seasons I have programmed installations dedicated to interactive learning responding to the exhibitions at VISUAL. These installations foreground learning through making and play, responding to themes of the exhibitions.
These installations were for children and adults and use diverse materials and approaches to practice. I wanted to develop convivial, intimate opportunities for co-constructive learning through artwork and to facilitate access to contemporary art for everyone, particularly children. I believe the possibility for learning is greatly heightened through the engagement of the senses, in embodied ways: exploring through physically touching, building, drawing, playing.
Through interactive installations, that are constantly changing and growing over the life of an exhibition, mutual learning can also happen between artists and members of the public who engage with their work.
It is always my goal to make spaces for risk taking and the pursuit of open inquiry in structured environments, interactive learning installations provide such environments.