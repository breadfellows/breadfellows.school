Title: index
Date: 2018/09/01
featured_image: images/visual_workshops/schools3.jpg
save_as: VISUAL_residency_index.html
rating: 5
Tags: galleries, schools, resources, publications, communities, breadfellows, research

# Working with VISUAL Center for Contemporary Art

In September 2018 I was invited to develop an artist residency at VISUAL Center for Contemporary Art focused on gallery-based learning and programming for young people supported by Creative Ireland and Carlow Arts Office. This residency aimed to address the lack of structured education programming at VISUAL.
In 2019, this residency was extended for a year and in 2020 I worked as Curator of Learning developing an online learning programme in response to the Covid 19 Pandemic.

As part of this residency I conducted Breadfellows' Chats with the staff at VISUAL, developed a series of workshops for schools and young people, programmed an interactive learning space called the Make Play Space, developed resources for teachers responding to the exhibitions, delivered workshops for toddlers and their adults, and developed an online learning programme.
