Title: Make an Exhibition at Home - Online Learning
Date: 2020/01/04
rating: 5
Featured_image: /images/online_learning/why_is_the_sky_Blue3.png
Homepage_summary: With the outbreak of the covid19 pandemic, I was invited to develop an online learning programme for VISUAL. This included the curation of a number of video series: "Homemade", "Make an Exhibition at Home" for Cruinniú na nÓg and a "Parent and Toddler" series produced by Siobhán Jordan.

Make an Exhibition at Home was a video series I curated and produced for VISUAL for their Cruinniú na nÓg programme, funded by the Carlow Arts Office and Creative Ireland.

![](/images/online_learning/exhibition2.jpg){.third-centered}
![](/images/online_learning/exhibition3.jpg){.third-centered}
![](/images/online_learning/exhibition4.jpg){.third-centered}
{.grid}

For this series I invited Siobhán, Alice and Charlotte Jordan, Jane Fogarty, David Lunney, Raluca Croitoru, Katherine Mac Bride, Tom and Fiadh Watt, Daniel Tuomey, Dan Fogarty, Laura Ní Fhliabhín, Mary Conroy, Stephane Hanley, Ruth Clinton, Niamh Moriarty, Sarah Devereux, Blaine O'Donnell and Bridget Flannery to talk about making exhibitions in their homes to inspire children to consider their home environment as a place to make and display art and to make exhibitions.
The theme for the project "why the sky is blue?" was taken from the summer programme at VISUAL and the legacy of the Carlow-based scientist John Tyndall.

<iframe width="560" height="315" src="https://www.youtube.com/embed/bbNIHH92Ezc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/t0aGcNW1xbw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

John Tyndall, helped to discover why the sky is blue. Children were invited to hold their exhibitions at home,the publica callout for entries to the competition read: 
"Your exhibition could be huge or very small, it could happen under your bed, in the garden, in the bath or take over an entire room of your house."

<iframe width="560" height="315" src="https://www.youtube.com/embed/IpSVqxMx7Bk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/0wBUl401zsU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

There were 20 submissions to the competition and the most creative exhibitions were awarded vouchers for art supplies.

<iframe width="560" height="315" src="https://www.youtube.com/embed/z0993_qLwDY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Project page at VISUAL can be found here
https://www.visualcarlow.ie/education/families/
