Title: Schools Workshops
Date: 2018/01/09
rating: 5
Featured_image: /images/visual_workshops/schools3.jpg
Homepage_summary: In September 2018 I was invited to develop an artist residency at VISUAL Center for Contemporary Art focusing on gallery-based learning and programming for young people supported by Creative Ireland and Carlow Arts Office.

#Workshops at VISUAL 

![](/images/visual_workshops/schools3.jpg)

In September 2018 I was invited to develop an artist residency at VISUAL Center for Contemporary Art focusing on gallery-based learning and programming for young people supported by Creative Ireland and Carlow Arts Office.
As part of this residency I developed workshops for school groups responding to three exhibitions, Marjeticia Potrc's exhibition Shelter: Open/ Closed; A Painter's Life: Stephen McKenna (1939 - 2017); and Artworks 2019: Dearly Beloved.

![](/images/visual_workshops/schools2.jpg)

The programme of workshops I developed in the gallery respond to exhibitions, and are committed to embodied forms of learning. Children had the opportunity to play with materials and to collaboratively build in the gallery space, often in a free-form way.

![](/images/visual_workshops/schools18.jpg)
![](/images/visual_workshops/schools27.jpg)

Children developed hands-on, active inquiry skills, employed design-thinking and explored the exhibitions through writing, conversation and drawing. The programme of workshops focused on learning through interaction with contemporary artworks in playful, welcoming, safe ways, that aimed to promote curiosity through active conversation, and to instill confidence in the gallery space. These programmes of workshops changed as children and teachers gave input and feedback.

![](/images/visual_workshops/schools22.jpg){.third-centered}
![](/images/visual_workshops/schools23.jpg){.third-centered}
![](/images/visual_workshops/schools14.jpg){.third-centered}
{.grid}

All age groups attended the workshops and exhibitions.

![](/images/visual_workshops/schools9.jpg)
![](/images/visual_workshops/schools29.jpg)