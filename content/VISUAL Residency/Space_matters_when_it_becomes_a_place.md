Title: Space matters when it Becomes a Place
Date: 2018/01/09
rating: 5
Featured_image: /images/space/P_lobby_07.jpg
Homepage_summary: Space matters when it Becomes a Place was an interactive installation responding to Marjeticia Potrc's exhibition Shelter: Open/ Closed at VISUAL

#Space Matters when it Becomes a Place 

This was an interactive installation responding to Marjeticia Potrc's exhibition Shelter: Open/ Closed at VISUAL

![](/images/space/P_lobby_07.jpg)

This installation offered a space to respond to Marjeticia Potrc's exhibition Shelter: Open/ Closed and provoked questions around how people build shelter and how we live together in proximity in open and closed cultures.

![](/images/space/P_lobby_01.jpg)

![](/images/space/P_lobby_03.jpg){.third-centered}
![](/images/space/P_lobby_08.jpg){.third-centered}
![](/images/space/P_lobby_09.jpg){.third-centered}
{.grid}

The installation used recycled materials to promote sustainability and offered an open, welcoming space to make constructions and to play.

![](/images/space/P_lobby_12.jpg){.third-centered}
![](/images/space/P_lobby_11.jpg){.third-centered}
![](/images/space/P_lobby_14.jpg){.third-centered}
{.grid}

The space was slowly filled with constructions, sculptures and drawings as the exhibition progressed. The resources for teachers and the programme of workshops I developed in the gallery alongside these installation responded to the themes of home, shelter, belonging and community. Children had the opportunity to play with materials and to collaboratively build in the gallery space in a free-form way. Children developed hands-on, active inquiry skills, employed design-thinking and explored the exhibition through construction, writing, conversation and drawing.

![](/images/visual_workshops/schools28.jpg)

For 5 seasons I have programmed installations dedicated to interactive learning responding to the exhibitions at VISUAL. These installations foreground learning through making and play, responding to themes of the exhibitions.
These installations were for children and adults and use diverse materials and approaches to practice. I wanted to develop convivial, intimate opportunities for co-constructive learning through artwork and to facilitate access to contemporary art for everyone, particularly children. I believe the possibility for learning is greatly heightened through the engagement of the senses, in embodied ways: exploring through physically touching, building, drawing, playing.
Through interactive installations, that are constantly changing and growing over the life of an exhibition, mutual learning can also happen between artists and members of the public who engage with their work.
It is always my goal to make spaces for risk taking and the pursuit of open inquiry in structured environments, interactive learning installations provide such environments.
