Title: Homemade - Online Learning
Date: 2020/01/04
rating: 4
Featured_image: /images/online_learning/homemade01.jpg
Homepage_summary: Online Learning Programme of videos with interactive pdfs

# Homemade - a video series at VISUAL

With the outbreak of the covid19 pandemic I was invited to develop an online learning programme for VISUAL. This included the curation of a number of video series: "Homemade", "Make an Exhibition at Home" for cruinniú na nÓg and a "Parent and Toddler" series produced by Siobhán Jordan.

![](/images/online_learning/homemade01.jpg){.third-centered}
![](/images/online_learning/homemade02.jpg){.third-centered}
![](/images/online_learning/homemade03.jpg){.third-centered}
{.grid}

Homemade was a video series I curated and produced that focused on sustainable practice at home, making art materials with things you might already have, such as food waste, used art materials and kitchen staples.
The video series can be viewed here [visualcarlow.ie](https://www.visualcarlow.ie/education/families/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/ksV0PKDRjlw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
