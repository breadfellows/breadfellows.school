Title: Still Life Studio
Featured_image: /images/still_life_studio/still_life9.jpg
Date: 2018/01/09
rating: 5
Homepage_summary: Still Life Studio was an interactive installation responding to the exhibition A Painter's Life: Stephen McKenna (1939 - 2017)

#Still Life Studio
Still Life Studio was an interactive installation responding to the exhibition 
A Painter's Life: Stephen McKenna (1939 - 2017)

![](/images/still_life_studio/still_life9.jpg)

This installation offered a space to respond to a diverse retrospective of the paintings of Stephen McKenna, bringing a focus to Stephen's lifelong committment to the discipline of still life drawing and painting.

![](/images/still_life_studio/still_life4.jpg)

The installation featured an array of objects similar to those used by Stephen in his own studies, an area for visitors to construct their own still lives, comfortable chairs with art materials and drawing boards. There were also many reference images and instructions for inspiration in how to approach making a still life study.

![](/images/still_life_studio/still_life1.jpg){.third-centered}
![](/images/still_life_studio/still_life2.jpg){.third-centered}
![](/images/still_life_studio/still_life3.jpg){.third-centered}
{.grid}

The space was slowly filled with drawings and paintings, made by all ages as the exhibition progressed. The resources for teachers and the programme of workshops I developed in the gallery alongside these installation explored the idea of an "artist's practice" as work; going to work everyday painting, as Stephen did. Thinking about artmaking as a job someone could do in the world, that takes time and committment. For instance, constantly returning back to the same objects and themes throughout your life as a way to explore an idea or object and its form in more depth. Children had the opportunity to play with many different drawing and painting materials and work individually and collaboratively.

![](/images/still_life_studio/still_life5.jpg){.third-centered}
![](/images/still_life_studio/still_life6.jpg){.third-centered}
![](/images/still_life_studio/still_life8.jpg){.third-centered}
{.grid}

For 5 seasons I have programmed installations dedicated to interactive learning responding to the exhibitions at VISUAL. These installations foreground learning through making and play, responding to themes of the exhibitions.
These installations were for children and adults and use diverse materials and approaches to practice. I wanted to develop convivial, intimate opportunities for co-constructive learning through artwork and to facilitate access to contemporary art for everyone, particularly children. I believe the possibility for learning is greatly heightened through the engagement of the senses, in embodied ways: exploring through physically touching, building, drawing, playing.
Through interactive installations, that are constantly changing and growing over the life of an exhibition, mutual learning can also happen between artists and members of the public who engage with their work.
It is always my goal to make spaces for risk taking and the pursuit of open inquiry in structured environments, interactive learning installations provide such environments.