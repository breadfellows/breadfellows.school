Title: A Collaborative Love Song
Date: 2018/01/09
rating: 5
Featured_image: /images/a_collaborative_love_song/love12.jpg
Homepage_summary: A Collaborative Love Song was an interactive installation responding to the Artworks 2019: Dearly Beloved exhibition at VISUAL.

A Collaborative Love Song was an interactive installation responding to the Artworks 2019: Dearly Beloved exhibition at VISUAL.

![](/images/a_collaborative_love_song/love12.jpg)

This installation offered a space for the public to contribute to a "collaborative love song" in response to the Summer Programme at VISUAL which included a group exhibition selected from an open submission responding to the Prince lyric "Dearly beloved, we are gathered here today to get through this thing called life". This installation was developed in collaboration with Colm O'Neill, with original music by Richard Breen.

![](/images/a_collaborative_love_song/love14.jpg){.third-centered}
![](/images/a_collaborative_love_song/VISClareBreen107.jpg){.third-centered}
![](/images/a_collaborative_love_song/VISClareBreen112.jpg){.third-centered}
{.grid}

The installation included a writing table, a recording booth and a stage for performance.

![](/images/a_collaborative_love_song/love11.jpg)
![](/images/a_collaborative_love_song/VISClareBreen108.jpg)
![](/images/a_collaborative_love_song/VISClareBreen111.jpg)

At the writing table, visitors could compose a lyric from a collection of the most commonly recurring words in the greatest love songs of all time. These words were collected with the help of a "lyric scraper", a computer programme written by Colm O'Neill that analysed 100s of love songs to come up with the most frequently occurring words.
For example, love was the most common word occurring 1453 times, followed by know, 507 times; yeah, 443 times; baby, 441 times.....now (334), wanna (279), time (271), never (255),one (243), day (243) want, make, take, life, see, heart, feel, girl, way.......

![](/images/a_collaborative_love_song/love02.jpg){.third-centered}
![](/images/a_collaborative_love_song/love05.jpg){.third-centered}
![](/images/a_collaborative_love_song/love08.jpg){.third-centered}

When the visitor had written their short lyric they could go to the recording booth and record their line, which would be added to an enormous rolling recording, brought together to form a "collaborative love song".

![](/images/a_collaborative_love_song/VISClareBreen106.jpg)

Visitors could also perform their song on the stage.

![](/images/a_collaborative_love_song/VISClareBreen114.jpg)

For 5 seasons I have programmed installations dedicated to interactive learning responding to the exhibitions at VISUAL. These installations foreground learning through making and play, responding to themes of the exhibitions.
These installations were for children and adults and use diverse materials and approaches to practice. I wanted to develop convivial, intimate opportunities for co-constructive learning through artwork and to facilitate access to contemporary art for everyone, particularly children. I believe the possibility for learning is greatly heightened through the engagement of the senses, in embodied ways: exploring through physically touching, building, drawing, playing.
Through interactive installations, that are constantly changing and growing over the life of an exhibition, mutual learning can also happen between artists and members of the public who engage with their work.
It is always my goal to make spaces for risk taking and the pursuit of open inquiry in structured environments, interactive learning installations provide such environments.
