Title: Follow the Rainbows - activities for the gallery
Date: 2020/01/07
rating: 4
Featured_image: /images/rainbows/rain01.jpg
Homepage_summary: Follow the Rainbows was an interactive installation responding to Artworks 2020: The Sky is Blue, the Summer exhibition at VISUAL

# Follow the Rainbows
A trail of hidden rainbows was installed all around the gallery using prisms, along with a card game asking questions about the exhibitions and makign space for drawing and interpretations of the artworks.

![](/images/rainbows/rain02.jpg)

![](/images/rainbows/rain04.jpg){.third-centered}
![](/images/rainbows/rain04.1.jpg){.third-centered}
![](/images/rainbows/rain05.jpg){.third-centered}

The project is inspired by the work of John Tyndall, a Carlow based scientist who made discoveries contributing to our understanding of climate change, the greenhouse effect and why the sky is blue.

![](/images/rainbows/rain03.jpg)

For 5 seasons I have programmed installations dedicated to interactive learning responding to the exhibitions at VISUAL. These installations foreground learning through making and play, responding to themes of the exhibitions.
These installations were for children and adults and use diverse materials and approaches to practice. I wanted to develop convivial, intimate opportunities for co-constructive learning through artwork and to facilitate access to contemporary art for everyone, particularly children. I believe the possibility for learning is greatly heightened through the engagement of the senses, in embodied ways: exploring through physically touching, building, drawing, playing.
Through interactive installations, that are constantly changing and growing over the life of an exhibition, mutual learning can also happen between artists and members of the public who engage with their work.
It is always my goal to make spaces for risk taking and the pursuit of open inquiry in structured environments, interactive learning installations provide such environments.