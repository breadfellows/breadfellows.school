Title: Breadfellows' Chats at VISUAL
Date: 2018/07/23
rating: 5
featured_image: images/visual_breadfellows/breadfellows_visual2.jpg
Homepage_summary: Breadfellows' Chats at VISUAL

I began my residency at VISUAL, Carlow in 2018 by meeting and talking with the staff from across all departments of the institution about their relationship to the building, what their work entails and how VISUAL supports them in this work. During these conversations we made companions together.

![](/images/visual_breadfellows/breadfellows_visual2.jpg){.third-centered}
![](/images/visual_breadfellows/breadfellows_visual1.jpg){.third-centered}
![](/images/visual_breadfellows/breadfellows_visual7.jpg){.third-centered}

These companions (ceramic objects) were displayed in the gallery as part of Shelter: Open/ Closed alongside the work of Marjeticia Potrc; they have been included in the exhibition Useful Structures and they have been installed permanently in the kitchen. They will remain at the gallery to be used as part of the "infrastructure for hosting", to share meals and facilitate artists and the public to come together to share conversation with food in the galleries, adding to the care and support that the building provides for visitors and those who work there.

![](/images/visual_breadfellows/breadfellows_visual3.jpg)

They were used to share food as part of Marjeticia Potrc’s World Café and the Workhouse Union Conference in October 2018.
They were used during a residency meal welcoming the 2019/2020 artists in residence at VISUAL.

![](/images/visual_breadfellows/breadfellows_visual14.jpg){.third-centered}
![](/images/visual_breadfellows/breadfellows_visual5.jpg){.third-centered}
![](/images/visual_breadfellows/breadfellows_visual8.jpg){.third-centered}

