Title: Workshops for toddlers and their adults
Date: 2019/01
rating: 5
Featured_image: /images/parent_and_toddler/toddler1.jpg
Homepage_summary: During my artist residency at VISUAL in 2018 and 2019 I led a weekly workshop for toddlers exploring materials through sensory play.

# Parent and Toddler Sensory Play Workshops

![](/images/parent_and_toddler/toddler1.jpg)

During my artist residency at VISUAL in 2018 and 2019 I led a weekly workshop for toddlers exploring materials through sensory play.

![](/images/parent_and_toddler/toddler2.jpg){.third-centered}
![](/images/parent_and_toddler/toddler3.jpg){.third-centered}
![](/images/parent_and_toddler/toddler6.jpg){.third-centered}
{.grid}

These workshops helped to develop motor skills, language skills and social skills and were generally exploratory and process orientated. Often the materials used were decided by the children including painting, drawing, clay, construction, lightplay. Some of the sessions included developing resources that coud be used again and again, such as painting a set of blocks that were used weekly as part of free play.

![](/images/parent_and_toddler/toddler4.jpg){.third-centered}
![](/images/parent_and_toddler/toddler8.jpg){.third-centered}
![](/images/parent_and_toddler/toddler12.jpg){.third-centered}
{.grid}

![](/images/parent_and_toddler/toddler9.jpg)