Title: Materials Matter
Date: 2020/08/08
rating: 2
featured_image: images/materials_matter/mm7.jpg
tags: research, galleries, schools, resources, publications,

# Materials Matter / Ábhar Ábhar

Is a collaborative research and education project with Mary Conroy and Laura Ní Fhlaibhín. Our individual practices share the common denominator of sustainability and environmental awareness within the process of art production.

We live in a time where we need to change our approach to production and consumption. We have all become disconnected from the mass produced products we use in our daily lives - artists and their materials are no exception. We share the desire to develop and disseminate a way of working that not only reflects our own personal commitment to material sustainability within our practices, but a desire to assist others to make art sustainably.

For 2020 we will research, test, develop and collate a series of methods and recipes for the production of natural and DIY art materials and tools. Our aim is to disseminate this research as a resource, to revive craft knowledge and the use of natural materials while working with low cost, locally sourced raw ingredients.

The project was funded by the first artlinks collaboration award from Wexford, Carlow and Kilkenny arts offices.
More information can be found at https://materialsmatter.ie/
