Title: A Cup of Tea
featured_image: images/a_cup_of_tea/016.png
rating: 2
Date: 2018/01/09
Tags: communities, breadfellows, galleries

# A Cup of Tea September 2018 – January 2019

I was invited to work as a curator for a community project called A Cup of Tea, facilitated by artist Liga Valge with 14 women who use the Forward Steps Family Resource Centre in Tullow, Carlow.

![](/images/a_cup_of_tea/cot01.jpg){.third-centered}
![](/images/a_cup_of_tea/cot02.JPG){.third-centered}
![](/images/a_cup_of_tea/cot03.png){.third-centered}
{.grid}

The website can be found at [acupoftea.ie](http://acupoftea.ie/)

The project brought together women of 6 different nationalities living in Tullow. Liga and I supported the group to explore integration, social inclusion, multi-culturalism while making ceramic tea pots, and sharing conversation and many cups of tea together.

<iframe src="https://player.vimeo.com/video/310096596" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/310096596">A Cup of Tea</a> from <a href="https://vimeo.com/user93661387">Lizzie Breen</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

The women were from various backgrounds and they met every week to make teapots, to discuss their relationships to the village they live in and to share tea and cake together. This project aimed to empower women in the community to develop relationships and to share experiences of community engagement. It proved to be incredibly generative space to solve issues together and share knowledge from past experiences of moving to a new community.
I co-ordinated the documentation of this project, built a website in collaboration with Colm O'Neill, made a video piece bringing together the work by the participating women.
The project was exhibited at the Forward Steps FRC, in Thrive Cafe, Tullow and at the VISUAL Center for Contemporary Arts and funded by Carlow Arts Office and Creative Ireland.
