Title: Sarah De Wilde - Week 12
Date: 2018/02/07
rating: 5
Featured_image: /images/living-arts-project/flamingo2.jpg

# Sarah De Wilde

![](/images/living-arts-project/chry10.jpg){.third-centered}
![](/images/living-arts-project/chry12.jpg){.third-centered}
![](/images/living-arts-project/chry14.jpg){.third-centered}
{.grid}

![](/images/living-arts-project/chry15.jpg){.third-centered}
![](/images/living-arts-project/chry2.jpg){.third-centered}
![](/images/living-arts-project/chry3.jpg){.third-centered}
{.grid}

![](/images/living-arts-project/chry4.jpg){.third-centered}
![](/images/living-arts-project/chry5.jpg){.third-centered}
![](/images/living-arts-project/chry6.jpg){.third-centered}
{.grid}

![](/images/living-arts-project/chry7.jpg){.third-centered}
![](/images/living-arts-project/chry9.jpg){.third-centered}
![](/images/living-arts-project/chry16.jpg){.third-centered}
{.grid}

Yannis gave Sarah some chrysanthemums one day and she began taking pictures of them. Each day the light was different and so the picture was different, she used the camera to capture all of these tiny differences, to show all of the beautiful, small changes the angles and the light made to them.
Sarah sometimes thinks of a photograph as a kind of “instant painting”.

Sarah looks carefully at things in her everyday life and uses the camera as a way to change her view of the world. With a change in light and frame, a small potted plant in your kitchen could become a huge palm in the jungle at night time.

![](/images/living-arts-project/sdw.jpg)

Sarah takes pictures all the time, looking very closely at things. She uses the frame of the lens, playing with focus, colour and light to transform everyday life into beautiful, exotic images.

Sarah once turned a whole gallery green.

![](/images/living-arts-project/greenroom2.jpg){.half-centered .image-process-halfwidth}
![](/images/living-arts-project/greenroom.jpg){.half-centered .image-process-halfwidth}

This played a trick on visitor's eyes, and they adjusted to try to take in more red colour. In the last room of the exhibition were photographs of flamingos. Everyone's eyes had adjusted to the green and so these flamingos were even brighter, brilliant pinks and reds!

![](/images/living-arts-project/flamingo0.jpg){.third-centered}
![](/images/living-arts-project/flamingo1.jpg){.third-centered}
![](/images/living-arts-project/flamingo2.jpg){.third-centered}
{.grid}

The class tried some exercises Sarah had suggested to help them understand and play with focus, the eyes focuses in the same way a camera does. They played with depth perception, focusing from one finger to the other. This exercise brought an awareness to the mechanics of how the eye looks. They also tried some blind drawing and framing exercises.

![](/images/living-arts-project/sarah1.JPG){.third-centered}
![](/images/living-arts-project/sarah2.JPG){.third-centered}
![](/images/living-arts-project/sarah3.JPG){.third-centered}
{.grid}

Still thinking about framing, the class chose and cut coloured gels and used them to view the room, to frame objects in the room and look at the room from a wide angle and in very close detail, imagining the frame like the lens of a camera.

![](/images/living-arts-project/IMG_7716.JPG)

The class took these gels and placed them on the windows. Using acrylic paint, they picked out the form of objects in the garden, looking at the surface of the gel and through it to the garden they could see beyond it.  

![](/images/living-arts-project/sarah6.JPG){.third-centered}
![](/images/living-arts-project/sarah10.JPG){.third-centered}
![](/images/living-arts-project/sarah11.JPG){.third-centered}
{.grid}

This collage of images grew and were layed over one another. Moving from things they could see in the garden the class added objects that weren't in the garden, painting layers of colour through and over one another, adding monkeys, neon signs, trucks and fairies to the garden.

![](/images/living-arts-project/sarah9.JPG)

![](/images/living-arts-project/sarah4.JPG){.third-centered}
![](/images/living-arts-project/sarah5.JPG){.third-centered}
![](/images/living-arts-project/sarah8.JPG){.third-centered}
{.grid}

![](/images/living-arts-project/sarah13.jpg)
![](/images/living-arts-project/sarah14.jpg)
![](/images/living-arts-project/sarah15.jpg)
![](/images/living-arts-project/sarah12.jpg)
