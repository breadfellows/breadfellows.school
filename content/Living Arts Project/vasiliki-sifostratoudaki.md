Title: Vasiliki Sifostratoudaki - Week 9
Date: 2018/01/16
rating: 5
Featured_image: /images/living-arts-project/vaso1.jpeg

# Vasiliki Sifostratoudaki

"Lets make time into a bread"

Vasiliki is a Greek artist living and working in Athens.

![](/images/living-arts-project/vaso2.jpeg){.image-process-halfwidth}

Vasiliki Sifostratoudaki makes sculptures, drawings, paintings, photographs, and books.
Vasiliki uses her artwork to investigate the world around her. She is constantly collecting, recording and arranging things she finds in her every day life.

![](/images/living-arts-project/vaso3.jpeg)
![](/images/living-arts-project/vaso4.jpeg)

Vasiliki always carries a pencil and paper with her. She thinks a lot about lines and drawing. She makes drawings over long periods of time, as a way to record passing time, a little bit like a diary.

![](/images/living-arts-project/Vaso.PNG)
![](/images/living-arts-project/vaso5.jpg)

Vasiliki wants to help us try to observe things in our everyday life a little bit more carefully together. Sometimes this is the most important thing an artist can do.

Vasiliki has sent a task to the class

# "lets make time into a bread"

Vasiliki presents bread dough to the class as a material. Bread is made up of water, salt, flour and yeast. These are found all over the world in people's everyday life. We will explore bread dough as a material through drawing and observation.


We began by looking and talking about Vasiliki's artwork, especially her drawing. We tried to imagine all of the things we could see in her drawings. And how she makes them, over a long time, constantly looking and adding dots and smudges and lines, like a calendar, everyday, until finally all of these lines join up together and look like a city or a mountain.

We moved to the hall and we loosened up for our day of drawing with an old game called exquisite corpse.
![](/images/living-arts-project/Vasil1.JPG)


![](/images/living-arts-project/Vasil2.JPG)

Everyone drew a head at the top of the page, then passed it along and the next person drew the shouldres and arms and the next person drew the tummy, etc... and passing our drawings around the circle,

![](/images/living-arts-project/Vasil12.JPG){.third-centered}
![](/images/living-arts-project/Vasil15.JPG){.third-centered}
![](/images/living-arts-project/Vasil23.jpg){.third-centered}
{.grid}

making a collection together of amazing and weird bodies.

![](/images/living-arts-project/Vasil8.JPG)

Then we talked about mark making. Vasiliki always carries two things with her, a pencil and a notebook. She might need to make a drawing at any time (she never carries a rubber) and this is really all she needs to make very beautiful drawings! We drew heavy lines and light lines, spots and dots and nicks and stripes and dashes and dabs and scratches and smooth, smooth swirls!

![](/images/living-arts-project/vasil25.JPG)

Then we began a drawing with geometry, thinking and listening quietly to the instructions and drawing along quietly:
##Draw a circle somewhere on the page, you can turn your page gently, find a place that looks good on your circle and balance a square on it, turn the page again and find somewhere to balance a triangle, then turn again and add a rectangle somewhere, continue to build up spaces together,

![](/images/living-arts-project/Vasil16.JPG)

![](/images/living-arts-project/Vasil6.JPG){.third-centered}
![](/images/living-arts-project/Vasil4.JPG){.third-centered}
![](/images/living-arts-project/Vasil5.JPG){.third-centered}
{.grid}

##keep turning your page, balancing the shapes one on top of one another, finding spaces to squeeze in little ones and filling big spaces with big ones,

![](/images/living-arts-project/Vasil3.JPG)
##floating shapes and solid shapes.
![](/images/living-arts-project/Vasil7.JPG)
##The shapes can begin to overlap and build up
![](/images/living-arts-project/Vasil17.JPG)
##Try to add a happy triangle, a nervous square, a quiet octagon, draw an angry shape, a sad shape, an itchy shape,
![](/images/living-arts-project/Vasil18.JPG)
##try to find some shapes within your shapes, using the marks we made earlier, bring in your spots and dots and nicks and stripes and dashes
![](/images/living-arts-project/Vasil19.JPG)
##and dabs and scratches and smooth, smooth swirls and try to find some new images you didn't notice before.
![](/images/living-arts-project/Vasil20.JPG)

Then we talked a little about flour and dough and bread. I had brought some dough, Vasiliki asked us to think about all of the components that make up bread dough. We used some flour as a material for collage.

![](/images/living-arts-project/Vasil21.JPG)

Then we looked at some raw dough and drew it. This was very different to drawing geometric shapes, these were wobbily, wiggily, soft, curved lines. The shapes curved and swerved and globbed and blobbed and were much gooier and messier than the geometric shapes. We tried to make soft lines, smudging the pencil with our fingers.

![](/images/living-arts-project/Vasil9.JPG)
![](/images/living-arts-project/Vasil10.JPG)
![](/images/living-arts-project/Vasil11.JPG)
![](/images/living-arts-project/Vasil13.JPG)


I left some dough behind in the classroom for the class to draw as it changed in the temperature of the room - growing and shrinking in the heat of the day until finally going hard and musty and rotten, decomposing and maybe even stinking! All of the drawings would become like a calendar documenting these changes.
