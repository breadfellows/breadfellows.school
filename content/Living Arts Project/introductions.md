Title: Introductions -   Week 1
Date: 2017/10/18
rating: 5
Featured_image: /images/living-arts-project/Tunic_workshop_square.png

![](/images/living-arts-project/Tunic_workshop.jpg){.image-process-halfwidth}
Before the first workshop I asked each child to consider for a couple of days:
# If you were not a human, what would you like to be?
## This could be an animal or an object, an alien or a monster, anything you can think of, but it should reflect some of your best qualities. (If this question is very difficult you can ask your friends for some help!)

This question proposes an alternative introduction that is not based on nationality, age, gender etc. it leaves space for improvisation, allowing us all to introduce ourselves on our own terms.
Working collaboratively the children drew around one another while lying on the ground to find their shape, the traced figure became the outline for a tunic. Each child then painted the animal/ object/ monster/ alien they had selected on the tummy of their tunic to wear over their uniform for the coming weeks.

![](/images/living-arts-project/footballs.JPG){.image-process-halfwidth}
![](/images/living-arts-project/mairead_and_unicorn.JPG){.image-process-halfwidth}
![](/images/living-arts-project/batman_and_cheetah.JPG){.image-process-halfwidth}
![](/images/living-arts-project/the_joker.JPG){.image-process-halfwidth}
![](/images/living-arts-project/rainbow.JPG){.image-process-halfwidth}
![](/images/living-arts-project/IMG_2627.JPG){.image-process-halfwidth}
