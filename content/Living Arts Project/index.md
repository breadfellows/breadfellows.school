Title: index
Date: 2017/10
rating: 5
featured_image: images/living-arts-project/danf26.JPG
save_as: living_arts_project_index.html
Homepage_summary: This website is a space for exchange and learning over the course of six months between 48 primary school children from 3rd and 5th class and 11 contemporary artists based around the world. <br><br>The project is called Breadfellows' Chats and it is part of a larger initiative called The Living Arts Project organised by the Wexford Arts Center and supported by the Arts Department of Wexford County Council and the Arts Council of Ireland.
Embedded_sound: <iframe width="100%" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/348273090&amp;color=%23ff5500&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true"></iframe>
List_avatars: /images/living-arts-project/cheetah.JPG
              /images/living-arts-project/football.JPG
              /images/living-arts-project/optimus_prime.JPG
              /images/living-arts-project/Po.JPG
              /images/living-arts-project/hurl.JPG
              /images/living-arts-project/tiger.JPG
              /images/living-arts-project/hawk.JPG
              /images/living-arts-project/lion.JPG
              /images/living-arts-project/swallow.JPG
              /images/living-arts-project/werewolf.JPG
              /images/living-arts-project/unicorn2.JPG
              /images/living-arts-project/unicorn.JPG
              /images/living-arts-project/troll.JPG
              /images/living-arts-project/sliotar.JPG
              /images/living-arts-project/rose.JPG
              /images/living-arts-project/robot.JPG
              /images/living-arts-project/robin2.JPG
              /images/living-arts-project/robin.JPG
              /images/living-arts-project/pupper.JPG
              /images/living-arts-project/peacock.JPG
              /images/living-arts-project/paintbrush.JPG
              /images/living-arts-project/mermaid.JPG
              /images/living-arts-project/labrador.JPG
              /images/living-arts-project/guitar.JPG
              /images/living-arts-project/guineapig.JPG
              /images/living-arts-project/greyhound.JPG
              /images/living-arts-project/fox.JPG
              /images/living-arts-project/football3.JPG
              /images/living-arts-project/fairy.JPG
              /images/living-arts-project/dog4.JPG
              /images/living-arts-project/dog3.JPG
              /images/living-arts-project/dog2.JPG
              /images/living-arts-project/console.JPG
              /images/living-arts-project/cat.JPG
              /images/living-arts-project/butterfly.JPG
              /images/living-arts-project/bed.JPG
              /images/living-arts-project/batman.JPG
              /images/living-arts-project/ball2.JPG
              /images/living-arts-project/ball.JPG
              /images/living-arts-project/Puppy.png
              /images/living-arts-project/wrestling.JPG

# The Living Arts Project
is a yearly residency organised by the Wexford Arts Center and supported by the Arts Department of Wexford County Council and the Arts Council of Ireland.
I was invited as an artist in residence to St Ibar's National School and over the course of six months I worked with 48 primary school children from 3rd and 5th class, their teachers, local potter and ceramic artist Mairead Stafford and 11 contemporary artists based around the world.

I selected them as they work with a wide range of media and concepts and because they were all very keen to learn from the children's interpretations of their work. Each week focused on the work of one artist. Some of the artists visited the class, some connected to the class via skype and some sent questions or artworks by post or email.
Each week, using this website the children talked about the work of an artist, they made something in response to their work and at the end of each workshop they considered the question “what does an artist do?”

The aim was to create a safe space, give a sense of continuity and to take the time to really converse, consider and contradict one another - it is a positive thing change our minds about what art is and what it is for.
