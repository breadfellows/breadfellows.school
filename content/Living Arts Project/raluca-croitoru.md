Title: Raluca Croitoru - Week 10
Date: 2018/01/24
rating: 5
Featured_image: /images/living-arts-project/raluca2.jpg

# Raluca Croitoru

![](/images/living-arts-project/raluca1.jpg)

Raluca is a Romanian artist living in Rotterdam. Raluca makes performances and films.

Raluca is interested in how we communicate with one another, not only through language but also in the way we move our bodies. Raluca likes to work together with other people, thinking about ways bodies can move separately and together.

![](/images/living-arts-project/raluca3.jpg)
![](/images/living-arts-project/raluca4.JPG)
![](/images/living-arts-project/raluca5.JPG)

Raluca's films, performances and workshops help to remind us that our minds and bodies are not separate. They help us think about the way the environment we live and work in effects our bodies and the way we communicate with one another.

![](/images/living-arts-project/raluca6.jpg)

Raluca visited the class fr her workshop and we began by looking at Raluca's performance and video work. Raluca led four exercises to help the children think about their bodies producing sound in relation to one another. The first was a simple game of introductions through their names and a sound. The second was a sound orchestra. For this, the class came up with different sounds in groups and Raluca conducted them as an orchestra.


<iframe src="https://player.vimeo.com/video/270617281" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/270617281">Raluca&#039;s Orchestra</a> from <a href="https://vimeo.com/user83575056">breadfellows.school</a> on <a href="https://vimeo.com">Vimeo</a>.</p>


The third exercise was a sound poem, half of the class had their eyes closed while the other half tiptoed around making strange noises into the ears of the rest. This produced intimate acoustic environments for each person.
For the fourth exercise the class split into groups and together came up with a soundscape for a place, then performed it for the other group, these included a supermarket, an airport, a school.
The workshop ended with a live foley recording, the class watched the stopmotion video they had made with Kari Robertson and performed created an audio soundscape together for the film.


<iframe src="https://player.vimeo.com/video/270616026" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/270616026">Making Foley Sound</a> from <a href="https://vimeo.com/user83575056">breadfellows.school</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

This is the complete film

<iframe src="https://player.vimeo.com/video/270611496" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/270611496">breadfellows_film</a> from <a href="https://vimeo.com/user83575056">clare</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
