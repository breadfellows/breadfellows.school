Title: Katherine MacBride - Week 5
Date: 2017/11/22
rating: 5
Featured_image: /images/living-arts-project/Katherine_mcbride.jpg

# Katherine MacBride

![](/images/living-arts-project/Katherine1.jpeg){.image-process-fullwidth}

Katherine is an artist who is interested in how people, animals, plants and things live together. At the moment she is working a lot with listening — listening with your ears to what other people are saying with their voices, listening with your body to what other people are saying with their bodies…listening with all your senses to pay really careful attention to how everything is all connected together and how our actions affect each other all the time even when we aren’t aware of it.
She’s trying to understand how to listen to things like the environment that might not have a voice and how to listen attentively so we can take good care of each other. Katherine often works to make things with other people.

![](/images/living-arts-project/Katherine3.jpeg){.image-process-fullwidth}

She likes to do things with words and language, but she also likes to make objects out of cloth and clay, and sometimes she likes to take photographs and make films.

![](/images/living-arts-project/Katherine2.jpeg){.image-process-fullwidth}

This week Katherine came to visit the school. We began with simple movement exercises: walking from point A to point B without bumping into one another, walking like robots, walking with very heavy feet, walking like your entire body was a magnet.

![](/images/living-arts-project/Katherine18.jpg){.image-process-fullwidth}

Then Katherine introduced herself and her work to the class and they introduced themselves and their work to her.
We began with a game designed to help us think about instructions, giving one another instructions and following instructions. Everyone made up an instruction for a body, for example: touch your toes, lick your nose, do 20 pushups, pretend to do a "number 2", sleepwalk.

![](/images/living-arts-project/katherine14.jpg){.image-process-fullwidth}
![](/images/living-arts-project/katherine15.jpg){.image-process-fullwidth}
![](/images/living-arts-project/katherine16.jpg){.image-process-fullwidth}

![](/images/living-arts-project/katherinenote1.jpeg){.third-centered .image-process-fullwidth}
![](/images/living-arts-project/katherinenote4.jpeg){.third-centered .image-process-fullwidth}
![](/images/living-arts-project/katherinenote3.jpeg){.third-centered .image-process-fullwidth}
{.grid}

![](/images/living-arts-project/katherinenote2.jpeg){.third-centered .image-process-fullwidth}
![](/images/living-arts-project/katherinenote5.jpeg){.third-centered .image-process-fullwidth}
![](/images/living-arts-project/katherinenote6.jpeg){.third-centered .image-process-fullwidth}
{.grid}

Each person took one of these instructions from the pile and acted it out. We performed these for one another.

![](/images/living-arts-project/katherine22.jpg){.third-centered .image-process-fullwidth}
![](/images/living-arts-project/katherine23.jpg){.third-centered .image-process-fullwidth}
![](/images/living-arts-project/katherine21.jpg){.third-centered .image-process-fullwidth}
{.grid}

We went through a number of other exercises that helped us think about our bodies in relation to one another and making sounds. We made loud sounds and soft sounds, we breathed loudly and quietly and tried to move together with our eyes closed.
At the end of the workshop we made a score together that mapped our work that day. Everyone took turns adding to the document and we tried to make a drawing that described our day, to add as many details as we could collectively think of. We worked together so we wouldn't forget anything.

![](/images/living-arts-project/katherine20.jpg){.image-process-fullwidth}
![](/images/living-arts-project/katherine19.jpg){.image-process-fullwidth}
