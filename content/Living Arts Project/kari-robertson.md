Title: Kari Robertson - Week 8
Date: 2018/01/10
rating: 5
Featured_image: /images/living-arts-project/kari4.jpg

# Week 8 - responding to the work of Kari Robertson

![](/images/living-arts-project/kari1.jpg){.third-centered}
![](/images/living-arts-project/kari2.jpg){.third-centered}
![](/images/living-arts-project/kari3.jpg){.third-centered}
{.grid}

Kari Robertson is an artist who mostly makes films, videos and sound but she sometimes makes objects as well. She is interested in how bodies move, and where our bodies really begin and end. She likes to make up strange or unusual characters and stories.

Kari plays with theater and staging to tell these strange and unusual stories. She does this for two reasons; because she wants to make people laugh and then think, and secondly, because she wants to better understand the people and the world around her.

![](/images/living-arts-project/kari4.jpg)
![](/images/living-arts-project/kari7.jpg)
![](/images/living-arts-project/kari8.jpg)
![](/images/living-arts-project/kari5.jpg)

This week Kari came to visit the school. We began with some exercises to warm up our bodies and our brains:
Walking around the room slowly....then walking like you are a crab....or a dog.....like you have an extra leg...... or you have no bones..... like you are an octopus.... or a slug..... like your feet are made of magnets.... or you are verrrry verrrrry old.

We moved into a circle sitting on the floor.  To help remember everyone's names Kari asked everyone to introduce themselves with their name and a gesture. Then Kari talked about the kind of artwork she makes and why.

![](/images/living-arts-project/kari9.JPG)
![](/images/living-arts-project/kari11.JPG)

We split into groups of 4/5 and passed out pieces of paper that had objects on them. Each group had to embody that object together.

![](/images/living-arts-project/kari10.jpg)
![](/images/living-arts-project/kari12.JPG)
![](/images/living-arts-project/kari14.JPG)

Kari had set up three slr cameras and the classes made a stopmotion animation together with lots of homemade playdough, planning a storyboard, making and shooting the short films in three groups over an hour.

![](/images/living-arts-project/stopmotion.PNG)

Recipe for playdough:
Ingredients
1 cup flour, 1 cup water, 2 tsp cream of tartar, 1/3 cup salt, 1 tbsp vegetable oil, gel food coloring
Instructions
1. Mix together all the ingredients, except the food coloring, in a medium saucepan.
2. Cook over low/medium heat, stirring. Once it begins to thicken, add the food coloring.
3. Continue stirring until the mixture is much thicker and begins to gather around the spoon.
4. Stir,stir,stir, allow to cool a little and knead together - PLAY!
