Title: Daniel Tuomey - Week 7
Date: 2017/12/06
rating: 5
Featured_image: /images/living-arts-project/dant_profile.jpg

# Week 7 - responding to the work of Daniel Tuomey

![](/images/living-arts-project/dant_profile.jpg){.half-centered .lightbox .image-process-halfwidth}

Daniel likes to look at ordinary things like tables and chairs, and think about how they've come to exist and how they're connected to the ways we live our lives alone and together.

![](/images/living-arts-project/dant6.JPG){.third-centered .image-process-halfwidth}
![](/images/living-arts-project/dant4.JPG){.third-centered .image-process-halfwidth}
![](/images/living-arts-project/dant5.JPG){.third-centered .image-process-halfwidth}
{.grid}

Drawing is a very important part of Daniel's practice.

![](/images/living-arts-project/dant7.jpg){.image-process-halfwidth}
![](/images/living-arts-project/dant8.JPG){.image-process-halfwidth}

Daniel sent a task to the class

# Draw a picture of something that you'd like to have in your sitting room when you grow up.
## It should be something that doesn't already exist. It shouldn't be anything magical, or anything you need to plug in. Don't write any words on the drawing!

Daniel is very interested in the answers to this question. In the studio Daniel asks himself questions like this one when he makes his artwork. Daniel thinks about desire, especially when he desires to draw something or make something. Drawing is a hard task for artists. Sometimes we try to draw or make something and it does not turn out the way we desire, does this make it a bad artwork? Sometimes drawing and making things is easier if we get other people involved, but this can make things turn out very differently to how we first pictured them. Sometimes this is OK.

We began the class by drawing. The class drew quietly, coming up with an image in response to Dan's questions.

![](/images/living-arts-project/dant22.JPG){.third-centered}
![](/images/living-arts-project/danscan5.jpg){.third-centered}
![](/images/living-arts-project/danscan1.jpg){.third-centered}
{.grid}

![](/images/living-arts-project/danscan2.jpg){.third-centered}
![](/images/living-arts-project/danscan3.jpg){.third-centered}
![](/images/living-arts-project/danscan4.jpg){.third-centered}
{.grid}

After about 10 minutes everyone put their drawing into a pile and picked out someone else's drawing. We sat back down and without talking to one another, everyone made a small sculpture of their selected drawing.

![](/images/living-arts-project/dant1.JPG){.third-centered}
![](/images/living-arts-project/dant10.JPG){.third-centered}
![](/images/living-arts-project/dant11.JPG){.third-centered}
{.grid}

![](/images/living-arts-project/dant13.JPG){.third-centered}
![](/images/living-arts-project/dant14.JPG){.third-centered}
![](/images/living-arts-project/dant17.JPG){.third-centered}
{.grid}

We used simple materials: plastiscine, matchsticks and lollipop sticks to focus on the form of the sculptures, trying to represent the integrity of the drawings and the fantastic forms.

![](/images/living-arts-project/dant18.JPG){.third-centered}
![](/images/living-arts-project/dant34.JPG){.third-centered}
![](/images/living-arts-project/dant35.JPG){.third-centered}
{.grid}

![](/images/living-arts-project/dant36.JPG){.third-centered}
![](/images/living-arts-project/dant37.JPG){.third-centered}
![](/images/living-arts-project/dant23.JPG){.third-centered}
{.grid}

![](/images/living-arts-project/dant2.JPG)
![](/images/living-arts-project/dant21.JPG)

![](/images/living-arts-project/dant15.JPG)
