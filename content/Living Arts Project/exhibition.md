Title: Exhibition - Week 14
Date: 2018/04/14
rating: 5
Featured_image: /images/living-arts-project/exhib01.JPG

# Exhibition at the Wexford Arts Center

![](/images/living-arts-project/exhib01.JPG)
On the 14th of April the children's work was installed at the Wexford Arts Center alongside the work of Orla Bates with Scoil Mhuire, Horeswood, New Ross; David Begley with St. Senan’s National School, Enniscorthy; and John Busher with St. Mary’s National School, Ballygarret, Gorey.

![](/images/living-arts-project/exhib02.JPG)
![](/images/living-arts-project/exhib03.JPG)

The children served food to the guests in the ceramics they had made during the breadfellows' chats workshops. The table cloths were made from drawings of their most delicious and most disgusting foods.

![](/images/living-arts-project/exhib04.JPG)

This desire game, produced during Daniel Tuomey's workshop included drawings made by the children and plastiscine that visitors to the gallery could work with in response to the children's drawings.

![](/images/living-arts-project/exhib05.JPG)

The Breadfellows' Film was made over two workshops.

<iframe src="https://player.vimeo.com/video/270611496" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/270611496">Breadfellows Film</a> from <a href="https://vimeo.com/user83575056">breadfellows.school</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

![](/images/living-arts-project/exhib06.JPG)
![](/images/living-arts-project/exhib07.JPG)
![](/images/living-arts-project/exhib08.JPG)
![](/images/living-arts-project/exhib09.JPG)

![](/images/living-arts-project/exhib06.JPG)
