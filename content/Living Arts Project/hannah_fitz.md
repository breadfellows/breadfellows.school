Title: Hannah Fitz - Week 13
Date: 2018/02/28
rating: 5
Featured_image: /images/living-arts-project/In_Light_of_the_Lamp_hannah(2).png

![](/images/living-arts-project/han5.jpg){.half-centered }

Hannah Fitz is an artist, she lives in Frankfurt some of the time and in Dublin the rest of the time. She makes sculptures and videos.

![](/images/living-arts-project/hannah1.jpg){.half-centered }

Hannah says this sculpture is a flower in a vase, or this a sculpture pretending to be a flower in a vase. The sculpture is made up of a lot of different materials... wire, wood, plaster, water, paint....
Imagine a sculpture is all of these different materials coming together and pretending to be a…

![](/images/living-arts-project/hannah2.jpg)

DOG!

In Hannah’s sculptures and videos there is always something that is trying to be like something else. Her sculptures are trying to be chairs and tables, hat stands and lamps, but they are always more like a picture of those things than the actual things.
Hannah makes sculptures pretending to be objects in the world, to help us think a little bit more carefully about all of these objects in our lives and the shapes they take.

![](/images/living-arts-project/hannah7.jpg)

Sometimes it is like a room of Hannah's sculptures are all pretending to be real objects in the world, but they look like drawings of objects!
Imagine if the sculpture is pretending to be a stool, but the sculpture doesn't know where the stool begins and ends. So the sculpture thinks the stool is also the shadow of the stool and it looks like this.

![](/images/living-arts-project/hannah3.JPG){.half-centered }

Hannah also likes to think about colour.
Hannah paints these sculptures with a colour, to show they are meant to be together. To show they are part of a set - they are different from all of the other things in the room, but they’re like one another.  
It also means that each sculpture means different things for each person, depending on the colour.

![](/images/living-arts-project/hannah6.jpg){.third-centered }
![](/images/living-arts-project/hannah1.jpg){.third-centered }
![](/images/living-arts-project/hannah11.jpg){.third-centered }
{.grid}

So the sculpture is pretending to be an object, and sometimes getting it a bit wrong, and then the colour comes along. The colour shows that the objects belong together, but it also reminds each different person of different things.

![](/images/living-arts-project/han2.jpg)

The class began by looking, thinking and talking about Hannah's work. They talked about colour and associations what do certain colours mean to each of us? What do they remind us of, how do they effect us, do they trigger different emotions?

![](/images/living-arts-project/han01.jpg)
![](/images/living-arts-project/han02.jpg)
![](/images/living-arts-project/han03.jpg)
![](/images/living-arts-project/han04.jpg)
![](/images/living-arts-project/han05.jpg)
![](/images/living-arts-project/han06.jpg)

The class were split into teams - the blue, yellow, red and green teams. When they had collectively brought their associations for each colour together they painted four household objects, a mop, a plant, a vase and a lamp. Thinking of how simple Hannah's sculptures often look, they tried to represent the objects through a few simple characteristics, thinking of what is essential to include if they want to recognise each object.

![](/images/living-arts-project/han07.jpg)
![](/images/living-arts-project/han08.jpg)
![](/images/living-arts-project/han09.jpg)
![](/images/living-arts-project/han10.jpg)
![](/images/living-arts-project/han11.jpg)
![](/images/living-arts-project/han12.jpg)
![](/images/living-arts-project/han13.jpg)

As the paintings accumulated a we talked about how different colours brought the objects together as groups with different associations, like sports teams or uniforms, the same objects painted in yellow, had a different feeling to those painted in blue.

![](/images/living-arts-project/han14.jpg)
