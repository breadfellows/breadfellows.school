Title: Dan Fogarty - Week 6
Date: 2017/11/29
rating: 5
Featured_image: /images/living-arts-project/Daniel_Fogartycomp.png
Embedded_sound: <iframe width="100%" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/365528681&amp;color=%23ff5500&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true"></iframe>
Test: testing?

# Week​ ​6 - Responding to the work of Dan Fogarty

![](/images/living-arts-project/Danf1.JPG){.third-centered}
![](/images/living-arts-project/danf4.jpg){.third-centered}
![](/images/living-arts-project/danf3.jpg){.third-centered}
{.grid}

Dan Fogarty makes paintings, sculptures, jewelry, clothing, magazines and furniture. In this workshop we will look at the ideas Dan is working on in his studio in Rotterdam right now. Dan posted some examples of work to the children before this class.

Dan makes things everyday and is always thinking about form in his everyday life, inside and outside the studio. He tries to look at things in the world differently. For him, being an artist is about having a certain mindset. In this workshop Dan invites the children to think about greeting cards.

![](/images/living-arts-project/danf2.jpg)
![](/images/living-arts-project/Danfogarty_workshopplan.jpg)

This workshop considers how thinking about daily forms in a very specific, personal way changes how we see them and possibly changes our relationship to objects in our daily lives and to one another.

![](/images/living-arts-project/Package.JPG){.third-centered}
![](/images/living-arts-project/danf3.JPG){.third-centered}
![](/images/living-arts-project/danf26.JPG){.third-centered}
{.grid}

Dan sent the classes a parcel, in it was a big card with a small story. We began by making a list of all the times we can think of that people give cards....birthdays, weddings, anniversaries, new babies, new homes, Christmas, get well soon.... We made the list as long as we could, then the class split into pairs and came up with ideas for their own giant cards, just like Dan’s paintings. The only limitation was that they couldn’t use any of the occasions on the list, they had to think of personal, specific reasons to send a card.

![](/images/living-arts-project/danf20.JPG)
![](/images/living-arts-project/danf19.JPG)
![](/images/living-arts-project/danf17.JPG)
![](/images/living-arts-project/danf18.JPG)
![](/images/living-arts-project/danf7.JPG)

![](/images/living-arts-project/Danf24.JPG){.third-centered}
![](/images/living-arts-project/danf10.JPG){.third-centered}
![](/images/living-arts-project/danf4.JPG){.third-centered}
{.grid}
