Title: Angelica Falkeling - Week 2
Date: 2017/10/25
rating: 5
Featured_image: /images/living-arts-project/Angelica_Pass_this_On.png
Embedded_sound: <iframe width="100%" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/353248676&amp;color=%23ff9900&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true"></iframe>

# Week 2 - responding to the work of Angelica Falkeling

![](/images/living-arts-project/Angelica_Pass_this_On.png){.half-centered }

Angelica is an artist who thinks about trust, identity and love and makes sculptures, films, clothes and performances. Angelica's workshop will be about pockets.

We will consider the utility of clothes. Who makes them and what are they for? Which clothes are fancy? Which are not? How do we dress for a brithday party? a job interview? a sleepover? a trip to the airport?

![](/images/living-arts-project/Angelica_pass_this_on_detail.jpg){.third-centered }
![](/images/living-arts-project/angelica_pocket_samples.jpg){.third-centered }
![](/images/living-arts-project/angelica_installation_shot.jpg){.third-centered }
{.grid}

What does it mean if our clothes are useful? Do we have pockets? How do pockets change our clothes? Are girls and boys clothes different and why?

Angelica designed a jacket as a gift for the feminist politician Soraya Post, we looked at this design, talked about the fabric, the shape and how it was made. After we talked through these questions the children returned to the tunics they had painted in the first weeks and added pockets, ties and cut them out so they could be worn. We thought of ways to make them more useful, more beautiful or more comfy. We also talked about what our clothes are for and what they say about us.

![](/images/living-arts-project/pocket5.JPG)
![](/images/living-arts-project/pocket7.JPG)
![](/images/living-arts-project/pocket15.JPG)
![](/images/living-arts-project/pocket16.JPG)
![](/images/living-arts-project/pocket19.JPG)
![](/images/living-arts-project/pocket12.JPG)
![](/images/living-arts-project/pocket3.JPG)
![](/images/living-arts-project/pocket4.JPG)
![](/images/living-arts-project/pocket10.JPG)
