Title: Clare Breen - Week 3 + 4
Date: 2017/11/8
rating: 5
Featured_image: /images/living-arts-project/breadfellows_companion.png

# Clare Breen

This project is called Breadfellows' Chats and it is about collaborative making, conversation and sharing, especially food.

![](/images/living-arts-project/companions.JPG){.image-process-halfwidth}

These ceramics are called companions. Companion means friend - someone or something you spend time with. The word companion is dervied from the Latin “com”, meaning with and the French “panis” meaning bread, so companion literally means "with bread"; sharing bread together, or breadfellows.

Companions are made by 2 people and they are part of a larger project, initiated by Clare Breen called the Breadfellows' Chats. In Breadfellows' Chats two people come together in conversation about art. This conversation inevitably touches every aspect of life. During this conversation they co-construct a ceramic object whose form is directly negotiated during this conversation - two sets of hands working together construct an object that holds within it the promise of a second meeting – to fulfil their practical funcion. The conversation that took place around the production of these objects is as important as the object itself. These objects are made by two people and are both artworks and functional tableware.

![](/images/living-arts-project/foundue2.jpg){.third-centered .image-process-halfwidth}
![](/images/living-arts-project/companionblue.jpg){.third-centered .image-process-halfwidth}
![](/images/living-arts-project/companiontracy.JPG){.third-centered .image-process-halfwidth}
![](/images/living-arts-project/companioncutlery.JPG){.third-centered .image-process-halfwidth}
![](/images/living-arts-project/companionkatinka.JPG){.third-centered .image-process-halfwidth}
![](/images/living-arts-project/companiondoublebowl.JPG){.third-centered .image-process-halfwidth}
{.grid}

Coming together and sharing food is an important part of Clare's practice. Sharing a meal is an moment for intimacy. During meals we converse and share, it is also a time when much informal learning takes place. Sharing meals is a convivial ritual practiced all over the world. Clare is interested in taking time to learn constructively through one another.

![](/images/living-arts-project/feast1.jpg){.image-process-fullwidth}
![](/images/living-arts-project/Feast2.JPG){.image-process-fullwidth}

In school we began by talking about food, delicious and disgusting food imaginging a feast we could share together with many delicious and disgusting dishes.

![](/images/living-arts-project/week4.2.JPG){.image-process-fullwidth}
![](/images/living-arts-project/week3.6.JPG){.image-process-fullwidth}
![](/images/living-arts-project/week3.7.JPG){.image-process-fullwidth}
![](/images/living-arts-project/week3.4.JPG){.image-process-fullwidth}

We then worked together to make companions that will be used to share a meal at the end of our project together.

![](/images/living-arts-project/week3.9.jpg){.image-process-fullwidth}
![](/images/living-arts-project/week3.10.JPG){.image-process-fullwidth}
![](/images/living-arts-project/week4.1.JPG){.image-process-fullwidth}
![](/images/living-arts-project/week4.10.JPG){.image-process-fullwidth}
![](/images/living-arts-project/week4.8.JPG){.image-process-fullwidth}
![](/images/living-arts-project/week4.9.JPG){.image-process-fullwidth}
![](/images/living-arts-project/week4.JPG){.image-process-fullwidth}
![](/images/living-arts-project/week3.13.JPG){.image-process-fullwidth}

![](/images/living-arts-project/breadfellows1.jpg){.third-centered .image-process-fullwidth}
![](/images/living-arts-project/breadfellows2.jpg){.third-centered .image-process-fullwidth}
![](/images/living-arts-project/breadfellows5.jpg){.third-centered .image-process-fullwidth}
{.grid}
