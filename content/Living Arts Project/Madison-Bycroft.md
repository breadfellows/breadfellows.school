Title: Madison Bycroft - Week 11
Date: 2018/01/31
rating: 5
Featured_image: /images/living-arts-project/Madi_profile.png
Embedded_sound: <iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/402575064&amp;color=%23ff5500&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true"></iframe>

# Madison Bycroft

![](/images/living-arts-project/Madi.png)

Madison is an Australian artist currently living between Paris and Adelaide. Madison makes performances, sculptures, videos, drawings, paintings and writes all about the possibilities of language. Madison makes art all the time, everyday, at the computer, in the studio, with other people or alone. Madison collects ideas from every place and is always questioning what it means to be an artist.

![](/images/living-arts-project/madi1.jpg)

Madison also thinks about bodies, different bodies and why our bodies are the way they are. Madison is especially interested in bodies that are different to human bodies, bodies that use their senses differently, and bodies that don't live in a human world and what our human bodies can learn from these other bodies.

Madison wonders about how we can communicate better with one another and form better friendships, not just with humans but with animals too.

![](/images/living-arts-project/madi2.jpg)

Madison sent a poem for us to try to translate

# Who knows what kinds of sounds there were before the dogs started barking. But once they had begun their bark it had begun. Who knows what kinds of sounds there were before the beginning. The dogs announce, the dogs herald. The hounding heralds, the heralding hounds. The dogs barked and were barked too.
#aaaaaaaaaaaaooooooooooooooouuuuuuuuu, aaaaaaaaaoooooooooooooooouuuuuuuuuu

![](/images/living-arts-project/madi5.JPG){.third-centered }
![](/images/living-arts-project/madi6.JPG){.third-centered }
![](/images/living-arts-project/madi7.JPG){.third-centered }
{.grid}

![](/images/living-arts-project/madi8.JPG){.third-centered }
![](/images/living-arts-project/madi9.JPG){.third-centered }
![](/images/living-arts-project/madi10.JPG){.third-centered }
{.grid}

![](/images/living-arts-project/madi11.JPG)

![](/images/living-arts-project/madi12.JPG){.third-centered }
![](/images/living-arts-project/madi13.JPG){.third-centered }
![](/images/living-arts-project/madi14.JPG){.third-centered }
{.grid}

![](/images/living-arts-project/madi15.JPG){.third-centered }
![](/images/living-arts-project/madi16.JPG){.third-centered }
![](/images/living-arts-project/madi17.JPG){.third-centered }
{.grid}
