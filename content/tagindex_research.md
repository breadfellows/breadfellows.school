Title: research
Date: 2020/08
rating: 1
save_as: research_index.html
featured_image: images/visual_breadfellows/breadfellows_visual12.jpg
Template: tag_index
Status: draft

# Working with Artists & Research

As a practice-led researcher I apply various artistic research methodologies to my work in different contexts.

<!-- below will be an index of all the articles tagged with the same tag as this pages title -->
