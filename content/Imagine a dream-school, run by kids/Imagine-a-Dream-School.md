Title: Imagine a dream school, run by kids
Date: 2019/01/10
rating: 4
featured_image: images/L.a.p.2/dream_school.jpg
tags: schools, galleries, resources, publications

# Imagine a dream school, run by kids
was a project I developed at Murrintown National school as part of the Living Arts Project, funded by Wexford Arts Office. It ran from September 2018 – May 2019

<iframe width="560" height="315" src="https://www.youtube.com/embed/YFQYx79FFe8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I was artist in residence in a primary school in Murrintown, Co. Wexford for 7 months developing the project with 58 children, aged 7 - 9. During weekly sessions we explored the notion of what a dream-school could be through drawing, painting, collage, ceramics, audio, score writing, book making, construction and performance. The project was inspired by Paul Maheke's game Imagine a Perfect School.
This project imagined an ideal space, "a dream school", because imagining or sketching the ideal version of a thing, a utopia, can help us to better understand how to work with what we have, and how to improve it.

![](/images/L.a.p.2/dream17.jpg)

![](/images/L.a.p.2/dream4.jpg){.third-centered}
![](/images/L.a.p.2/dream9.jpg){.third-centered}
![](/images/L.a.p.2/dream5.jpg){.third-centered}
{.grid}

The project included a research phase, time dedicated to encountering contemporary artists Jane Fogarty and Blaine O'Donnell, who visited the school, and reflecting on their work, producing finished works and planning an exhibition. It was my aim to show that art is not just a thing it is something that happens and it is a way to help us learn with and from one another about the world weinhabit. Each week we explored new processes in order to dig deeper into what a dream-school, run by kids, might consist of. This included considering what children would learn in this school, what kinds of spaces the school would contain, what children would wear there and when they would go there.
The project was responsive to the children's ideas and I planned three weeks ahead, and based the progress on what the children were interested in doing next. The children used collaborative approaches to map the social and physical infrastructure of their school and to imagine another utopian version of a school run by children. This project culminated in an exhibition at Wexord Arts Center.


![](/images/L.a.p.2/dream12.jpg)
![](/images/L.a.p.2/dream13.jpg)
![](/images/L.a.p.2/dream14.jpg)