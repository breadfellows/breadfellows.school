Title: schools
Date: 2020/08
rating: 1
save_as: schools_index.html
featured_image: images/schools_workshops/eva_school.JPG
Template: tag_index
Status: draft

# Working with Schools

In my practice I aim to empower children as researchers, collaborators, explorers, and experts and to recognise children as a social group. Much of my work is focused on relational and collaborative practice. Relational practice is respectful and attendant to the power and potential of all relationships within learning environments, it is about giving time to children, cultivating and nurturing interactions and it is, by its nature, reflective.
Art is a tool to empower in the classroom, to aid linkage between subjects and experimentation. It is also an embodied focus on the process of making, on free experimentation, the exploration of materials and of self-directed discovery. I believe that art can empower children to think through ideas in an expanded way, to bring the whole body of a child into the learning, recognising the cognitive, social and emotional needs and recognising a culture of children.

Art is a means to teach the creative habits of the mind which include to be inquisitive, to be persistent, to be imaginative, to be disciplined and to be collaborative. In the words of Paul Collard, of Creativity, Culture and Education, “Creativity makes it possible for us to play with ideas, quickly and flexibly adapt to changed circumstances, take time to consider what to do next, resist temptations, stay focused, and meet novel, unanticipated challenges”.

Play is an essential right in this process. We are complicated beings, formed by our unique personal experiences and associations. Art can be a means to explore and understand these rich complexities and contradictions and children are expert at playful exploration. As an artist I am constantly learning from children's approaches to the projects I propose.

<!-- below will be an index of all the articles tagged with the same tag as this pages title -->
