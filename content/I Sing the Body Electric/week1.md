Title: I Sing the Body Electric - week 1
Date: 2018/01/26
rating: 4
Featured_image: /images/I-sing-the-body-electric/week1/wk1.12.jpg

# Breadfellows' Chats

I began the series of four workshops with Breadfellow' Chats. This is a practice of collaboratively making ceramics to use to share a meal, it foregrounds talking, making together and sharing food as intimate gestures of care.

![](/images/I-sing-the-body-electric/week1/w1.01.JPG)

We began the Breadfellows' Chats with a simple, collaborative drawing exercise. The children listed and drew food - they thought about sharing a meal together comprised of the most delicious, disgusting or fantastic food they could imagine.

![](/images/I-sing-the-body-electric/week1/wk1.03.jpg){.third-centered}
![](/images/I-sing-the-body-electric/week1/wk1.04.jpg){.third-centered}
![](/images/I-sing-the-body-electric/week1/wk1.05.jpg){.third-centered}
{.grid}

Working in pairs they shaped the clay into collaboratively made ceramic "companions" of their own design. These ceramics are called companions. Companion means friend - someone or something you spend time with.

![](/images/I-sing-the-body-electric/week1/wk1.06.jpg)

The word companion is dervied from the Latin “com”, meaning with and the French “panis” meaning bread, so companion literally means "with bread"; sharing bread together, or breadfellows.

![](/images/I-sing-the-body-electric/week1/wk1.07.jpg)
![](/images/I-sing-the-body-electric/week1/wk1.08.jpg)
![](/images/I-sing-the-body-electric/week1/wk1.09.jpg)

![](/images/I-sing-the-body-electric/week1/wk1.10.jpg){.third-centered}
![](/images/I-sing-the-body-electric/week1/wk1.11.jpg){.third-centered}
![](/images/I-sing-the-body-electric/week1/wk1.12.jpg){.third-centered}
{.grid}

![](/images/I-sing-the-body-electric/week1/wk1.13.jpg)
![](/images/I-sing-the-body-electric/week1/wk1.14.jpg)
