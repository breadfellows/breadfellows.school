Title: I Sing the Body Electric - week 4
Date: 2018/03/9
rating: 4
Featured_image: /images/I-sing-the-body-electric/Week4/w4.17.JPG

![](/images/I-sing-the-body-electric/Week4/w4.06.JPG)

On the 4th week we hosted an exhibition in the classroom which was attended by parents, grandparents and the rest of the school. The exhibition included the ceramics made in week 1, filled with food; drawings and sculptures responding to EVA's
themes of electricity and the body; texts written in response to artworks in EVA and images of their performances.

Over the course of four weeks, the classroom became a space for open dialogue, performance and a studio and exhibition space.

![](/images/I-sing-the-body-electric/Week4/w4.05.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.08.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.09.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.10.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.11.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.12.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.13.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.14.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.15.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.16.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.17.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.18.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.19.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.20.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.21.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.22.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.23.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.24.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.25.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.26.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.27.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.28.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.29.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.31.JPG)
![](/images/I-sing-the-body-electric/Week4/w4.32.JPG)
