Title: I Sing the Body Electric - week 2
Date: 2018/02/09
rating: 4
Featured_image: /images/I-sing-the-body-electric/week2/wk2.01.JPG

![](/images/I-sing-the-body-electric/week2/wk2.01.JPG)

In the second week I introduced the role of the curator to the children we talked about what a curator does. We looked at some of the artworks in EVA through the eyes of a curator, trying to understand what these artworks are and what they need to be happy in the world.

![](/images/I-sing-the-body-electric/week2/wk2.05.JPG)

The following is an excerpt from our conversation

Clare : Does anyone know what a curator is?

Children: no

Clare : Does anyone know what an exhibition is?

Children: Yes, they're in museums..... an exhibition is when you've done all your hard work and you want to show it off to people, what you've done.

Clare : So in my opinion, what a curator does is a very important job. Curators take care of artists and artworks. Do any of you take care of something?

Children: My dog....my little brother... babys...rabbits.....animals....your family....pets.... baby cousins.... your belongings.

Clare : And is it easy or difficult to take care of something?

Children: Hard..... easy..... especially if you love it alot. That means its easy. For a baby, you have to take care for a long, long time.

Clare : Does anyone take care of you?

Children: Mam...dad....nanny and grandad....friends....teachers

Clare : If you really want to take care of something you have to do a lot of work,what kinds of things do you have to do?

Children: Giving it food, giving it somewhere to sleep.... keeping it clean.... tickling it, playing with it....giving it clothes

Clare : You think about all the needs of the thing you take care of. So taking care of artworks is sort of the same. Curators read about artworks, they research them, they think about what they need, where they like to be, they decide where artworks should go. What do you think an artwork wants most in the world?

Children: For people to see it...to appreciate all of your hard work

Clare : If you were an artwork what would you like most?

Children: For people to see you and love you and buy you...... to admire you.

Clare : So not all artworks are beautiful, sometimes they are sad, sometimes they are supposed to make people angry, or to think about something they never thought of before. Sometimes you have to think very carefully at an exhibition. Curators spend all their time caring for artworks and thinking about them. Its a curators job to look very closely at the artwork, to decide what angle it looks best from, to decide when to put things together and when to leave something with lots of space.

Children: So curators are like Mammies of artworks? Or like teachers?

Clare : Yes, in ways I think so. Like any person who takes care of something.

![](/images/I-sing-the-body-electric/week2/wk2.06.JPG)

After these conversations the children came together in groups and enacted some of these artworks with their bodies, these included works by

Sean Keating,

![](/images/I-sing-the-body-electric/week2/wk2.15.jpg){.half-grid}
![](/images/I-sing-the-body-electric/week2/The_night_candles_are_burnt_out_sean_keating.jpg){.half-grid}
{.grid}

![](/images/I-sing-the-body-electric/week2/wk2.08.JPG){.third-centered}
![](/images/I-sing-the-body-electric/week2/Patrizio_Di_Massimo_Mum_2014.jpg){.third-centered}
![](/images/I-sing-the-body-electric/week2/wk2.09.JPG){.third-centered}
{.grid}

Mainie Jellet,

![](/images/I-sing-the-body-electric/week2/wk2.04.JPG){.half-grid}
![](/images/I-sing-the-body-electric/week2/mainie_jellet.PNG){.half-grid}
{.grid}

John Gerrard,

![](/images/I-sing-the-body-electric/week2/wk2.11.JPG){.half-grid}
![](/images/I-sing-the-body-electric/week2/2014_SolarReserve_Lincoln_Gerrard_02_Ewing.jpg){.half-grid}
{.grid}

Sam Keogh,

![](/images/I-sing-the-body-electric/week2/wk2.14.JPG){.half-grid}
![](/images/I-sing-the-body-electric/week2/Sam_Keogh_Captain_Cadaverine.PNG){.half-grid}
{.grid}

and Isabel Nolan.

![](/images/I-sing-the-body-electric/week2/wk2.13.JPG){.half-grid}
![](/images/I-sing-the-body-electric/week2/isabel.PNG){.half-grid}
{.grid}
