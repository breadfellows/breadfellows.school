Title: index
Date: 2018/01/26
rating: 4
featured_image: images/I-sing-the-body-electric/Week4/lightning.png
save_as: I_sing_the_body_electric_index.html
Homepage_summary:  I Sing the Body Electric was a 4 week project with 3rd and 4th class at Raheenagh National School, Limerick. It was part of EVA International 2018's school programme curated by Jennie Guy's Art School.
Tags: schools, galleries,

Jennie Guy was invited, through her project Art School, to develop a schools programme introducing children to the concept of the curator and the ecology of exhibitions that make up EVA International. Jennie was particularly drawn to the Sean Keating painting The Night Candles Are Burnt Out which led to a reconsideration of the Walt Whitman poem I Sing the Body Electric. An interpretation of this poem is that it gives focus to the complexity of the human body and its own inherent ecosystems.

Jennie invited me, through Art School, to respond to this title, thinking through the poem, its themes and the upcoming exhibition to develop a 4 week programme at Raheenagh National School, in Limerick. 

Through collaborative making and conversation, responding to the artworks and concepts of EVA International I developed four workshops, culminating in an exhibition in Raheenagh National School.

Children give meaning to the world around them through the development of narratives. Constructively learning, learning through listening to one another and developing tools for encountering the world together brings an awareness that the world is something shared. I developed 4 workshops which were focused on collaborative making, collective thinking and curation as a form of care.

I Sing the Body Electric occurred before EVA opened. From the outset I knew it was possible that the children might not see the exhibition. For this reason I was keen to engage with the themes and artworks of EVA, but to develop a project that could work for the children in their classroom as a stand-alone creative experience.

Within an ecology of learning with and from an exhibition on this scale, I consider the conversations and artworks generated in the classroom by the children in response to EVA's themes and artworks to be as valuable as any other aspect of the public programme. For this reason, I understood the space of the classroom as an extension of EVA, like a little satellite of learning orbiting the exhibition, generating new responses and artworks, even if in this instance, we were just an audience of ourselves.
