Title: I Sing the Body Electric - week 3
Date: 2018/02/23
rating: 4
Featured_image: /images/I-sing-the-body-electric/week3/wk3.05.JPG

Between the 2nd and 3rd week the children made their own artworks inspired by and responding to the EVA artworks and the themes of electicity and the body. On the third week we gathered together all of the work the children had made and developed an exhibition for their classroom. All decisions about this exhibition from the title of the show, to the layout of the room, the positioning of the works and the food served at the exhibition were made by the children.

![](/images/I-sing-the-body-electric/week3/wk3.01.JPG)
![](/images/I-sing-the-body-electric/week3/wk3.02.JPG)
![](/images/I-sing-the-body-electric/week3/wk3.03.JPG)
![](/images/I-sing-the-body-electric/week3/wk3.04.JPG)

They made plans of the classroom, maquettes of the artworks and decided together how they could change the classroom space and how to best works and accommodate visitors to their exhibition.

![](/images/I-sing-the-body-electric/week3/wk3.05.JPG)
![](/images/I-sing-the-body-electric/week3/wk3.06.JPG)
![](/images/I-sing-the-body-electric/week3/wk3.07.JPG)
