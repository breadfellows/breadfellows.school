Title: Curious Minds
Date: 2019/11/11
rating:4
featured_image: images/curious_minds/book11.png
Tags: schools, publications, resources,

# Curious Minds

![](/images/curious_minds/curiousminds.png)

Curious Minds is a publication project developed out of the Living Arts Project at Wexford Arts Center. The booklets are a free, printable resource, primarily meant for school teachers, however, they could be used by anyone with an interest in art and education in other contexts: artists, parents, people who run workshops, art institutions, etc. The content is organised in such a way that allows for flexibility. Most lessons are suitable for a diverse range of ages, from 1st class to 6th class.
You can download these resources from the Living Arts Website 
[livingartsproject.ie](https://livingartsproject.ie/)

![](/images/curious_minds/mirror.png)

I was invited by Karla Sanchez and Els Dietvorst to contribute to a resource they were developing for teachers as an extension of the Living Arts Project. Initially I contributed a series of lesson plans along with three other artists, Laura Ní Fhliabhín, David Begley and Orla Bates. After this, I illustrated the project in collaboration with the designer Colm O'Neill.

![](/images/curious_minds/KIDS.png){.third-centered}
![](/images/curious_minds/bee4.png){.third-centered}
![](/images/curious_minds/books.png){.third-centered}
{.grid}

There are 5 books, a "foundations" resource that gives exercises for warming the body up, winding the body down and focusing attention. The other four books are organised under the seasons Spring, Summer, Autumn and Winter, with activities appropriate to that time of year.

![](/images/curious_minds/signs5.png){.third-centered}
![](/images/curious_minds/book11.png){.third-centered}
![](/images/curious_minds/COMPANIONS.png){.third-centered}
{.grid}

<iframe width="560" height="315" src="https://www.youtube.com/embed/YFQYx79FFe8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe src="https://player.vimeo.com/video/290640691" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/290640691">Breadfellows&#039; Chats</a> from <a href="https://vimeo.com/aieportal">Arts in Education Portal</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

