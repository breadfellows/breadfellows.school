Title: Yellow Brick Residency
Date: 2017/06
rating: 4
featured_image: images/yellowbrick/yellowbrick7.JPG
Tags: galleries, communities, research, resources,

# Residency at Yellow Brick Project Space
Athens, April 2017.

![](/images/yellowbrick/yellowbrick.jpg)

While working at documenta 14, I undertook a residency at Yellow Brick Project Space with the support of the Irish Arts Council. During that time I conducted Breadfellows'Chats with artists, colleagues on the documenta 14 education team, and a group of refugee children.
As a gesture of gratitude, the ceramic companions produced during the residency became a set to be used at the gallery as part of its "infrastructure for hosting", when each new residency begins, the artists are welcomed with a meal, using these ceramic companions.
The project was also presented during Open Form, an open studio and conversation platform initiated by the curator Denise Araouzou.

![](/images/yellowbrick/yellowbrick1.jpg){.third-centered}
![](/images/yellowbrick/yellowbrick7.JPG){.third-centered}
![](/images/yellowbrick/yellowbrick8.jpg){.third-centered}
{.grid}

The event consisted of video and ceramic installations, a presentation and conversation about the nature of the practice of Breadfellows' Chats, the public were also free to engage in their own Breadfellows' Chats in the gallery.
Website available at : https://openformathens.hotglue.me/?Clare%20Breen

![](/images/yellowbrick/yellowbrick2.PNG)

These ceramic companions were included in the Under the Mango Tree programme: an international conference of artist-led learning initiatives curated by Sepake Angiama as part of documenta 14.
Website available at : http://www.yellowbrick.gr/STEPs/Step-3-Breadfellows-Chats

![](/images/yellowbrick/yellowbrick9.1.jpg){.third-centered}
![](/images/yellowbrick/yellowbrick9.2.jpg){.third-centered}
![](/images/yellowbrick/yellowbrick9.jpg){.third-centered}
{.grid}
