Title: Lay of the Land - An Port Residency
Date: 2020/01/01
rating: 3
featured_image: images/lay_of_the_land/lotl1.jpg
Tags: research, breadfellows, publications,

# Lay of the Land - An Port Residency
July 2019

![](/images/lay_of_the_land/lotl1.jpg)

Lay of the Land is a series of site-responsive events taking place in outdoor places in Ireland. In July 2019 I was invited by Lay of the Land, Kari Cahill and Hazel McCague, to take part in a residency in An Port, Donegal. This was a week-long stay in a very remote cottage off the coast of Donegal. This residency offered the time to consolidate research I had been working on through various residencies and projects and to begin a new body of research into the use of more sustainable materials for art making including inks, dyes, paper and clay.

![](/images/lay_of_the_land/lotl2.jpg){.third-centered}
![](/images/lay_of_the_land/lotl3.jpg){.third-centered}
![](/images/lay_of_the_land/lotl4.jpg){.third-centered}
{.grid}