Title: methodology
Date: 2017/10/07
status: hidden

# Methods


The framework of this site has been built in collaboration with Colm O'Neill, a webdeveloper, opensource software researcher and a member of OSP, an Open Source Publication studio in Brussels. Colm has developed a framework for this site which uses free software and freely publishes all the code used to build it. [You can find it here](https://gitlab.com/breadfellows) Breadfellows.school is at once a free online pedagogical tool and a research and publication space.
