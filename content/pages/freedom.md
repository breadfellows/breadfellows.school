Title: freedom, licencing and disemination
Date: 2018/01/21
Status: hidden
Save_as: free-as-in-speech.html

# Open ? Free ? Copyleft ?

The design of this online space has been developed with the ethos of the Breadfellows' Chats methodology in mind. As a web designer Colm is focused on making the innerworkings of the sites he designs freely accessible, sharing all source code; the entire framework of the website is free for any person to access, study and duplicate.

Educational methods are (thankfully) rarely considered to be [intellectual property](https://en.wikipedia.org/wiki/Intellectual_property) however, documentation material, publications, artworks or media too easily gets bound, closed and made essentially unusable through copyright. This means that (outside of the academic frame) it is hard to use and publish research and methodological source materials in support of new educational research. As the focus of Breadfellows.school is to share and disseminate neoteric educational practices, we publish the written content, the visual material, the supporting audio, video media and the code developed specifically for this site under a copyleft licence. This is a statement of where the research positions itself socially. Copyright and licencing issues were not initial interest in the Breadfellows chat project, but through our collaboration on this platform we put forwards the potential space for education and free information to exist especially within the artistic fields.

< technical ligo >This site is rendered via the static site generator [Pelican](http://getpelican.com) converting Markdown files into static html files through a series of [Jinja2](http://jinja.pocoo.org/) templates. The result is a portable, versioned, easy to publish set. Clare uses the Gitlab web interface to enter and edit the page content in the very same repository in which all of the code to generate the website is kept and maintained. The site is deployed to a staging version via gitlab pages onto https://breadfellows.gitlab.io/breadfellows.school/ Later, a php script is manually triggered by us when an update is ready to go live.
< / technical lingo >

The collaboration on this project has functioned on several levels. Ideologically, the attitude of publishing this content freely ([as in speech](https://en.wikipedia.org/wiki/Gratis_versus_libre)) technically and legally echo the aims the Breadfellow chats. Meanwhile, this has also been an attempt at demystifying some of the technical or structural aspects necessary to build and publish a website. By using the exact same repository, by thinking about what visible and invisible interfaces both content writer and web designer have to use, we hope to establish a larger vocabulary for exchanging and building this website-publication-project. The goal is not to learn/teach all the skills necessary to build this site, but simply by making them visible, by documenting them, we make the system tangible for ourselves, and hopefully somewhat tangible to you too.

The breadfellows.school repository is available for you to consult, study, modify and reuse at [gitlab.com/breadfellows/breadfellows.school](https://gitlab.com/breadfellows/breadfellows.school) you can [download a snapshot of the entire system](https://gitlab.com/breadfellows/breadfellows.school/repository/master/archive.zip) for offline use.

Keep in mind that most of these ideas about freedom of information and alter-default licencing exist for quite some time now. We are applying them in this context but in doing so, we reference a rich tradition of [copyleft](https://en.wikipedia.org/wiki/Copyleft) practices.
