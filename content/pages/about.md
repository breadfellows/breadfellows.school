Title: about Clare Breen
Date: 2017/10/07

![](/images/living-arts-project/clare_profile.png)

My name is Clare Breen and I am an artist and learning curator currently based in Carlow.

I work in many different contexts but often I use Breadfellows' Chats, an artistic research methodology that grew out of my Master's research. It began as a practice to develop learning programmes for the gallery but it is constantly growing and changing. During Breadfellows' Chats I meet with people to talk about their work, during these chats we work with clay together to produce ceramic objects. These objects are called companions because they become a companion to the conversation and to the relationship. They are physically made by two people to be used to share a meal.

The word companion has its roots in Latin and French, “com” meaning with and “panion”, from the French panis, meaning bread; so companion literally means sharing bread together or breadfellows.

This methodology was initially developed to learn more about an artist's practice in order to develop learning programmes that respond to the questions at stake for the artist. I believe it is important to take time to understand an artist's work in order to build a responsive project that is embedded in a meaningful way in the artist's practice and to create a strong foundation for future learning.

The framework of this site has been built in collaboration with Colm O'Neill, a webdeveloper, opensource software researcher and a member of OSP, an Open Source Publication studio in Brussels. Colm has developed a framework for this site which uses free software and freely publishes all the code used to build it. [You can find it here](https://gitlab.com/breadfellows) Breadfellows.school is at once a free online pedagogical tool and a research and publication space.
