Title: Philosophy for Children Workshops
featured_image: images/tbgs/philosophy/art-philosophy05.jpg
rating: 4
Date: 2018/06/23
rating: 3

# Art and Philosophy Workshops - responding to the work of Otobong Nkanga and Hannah Fitz at Templebar Gallery and Studios in collaboration with Learning + Public Engagement Curator Katy Fitzpatrick
and Professor of Philosophy of Education Dr Aislinn O’Donnell.

These workshops were developed in close collaboration with the artists and using philoshphical enquiry methodologies developed by Katy Fitzpatrick and Aislinn O'Donnell.
I developed activities that explored the exhibitions through making, using various materials (clay, drawing, costumes) in playful conversations.

![](/images/tbgs/philosophy/art-philosophy05.jpg)

Art and Philosophy is an interdisciplinary pedagogical project that reflects on and unpacks contemporary visual art, through philosophical inquiry and arts and gallery education practices.

![](/images/tbgs/philosophy/art-philosophy01.jpg){.third-centered }
![](/images/tbgs/philosophy/art-philosophy02.jpg){.third-centered }
![](/images/tbgs/philosophy/art-philosophy03.jpg){.third-centered }
{.grid }

We also employed VTS, games from Theatre of the Oppressed and other philosophy games developed by Katy and Aislinn. At the centre of this project is the voice of the child, and through the process children developed their critical, philosophical and aesthetic capacities and sensibilities.
One of these workshops was developed as part of Crinniú na nÓg, a day for children's creativity across Ireland.

![](/images/tbgs/philosophy/tbgs_p4c3.jpg)
![](/images/tbgs/philosophy/tbgs_p4c4.jpg)


