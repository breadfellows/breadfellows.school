Title: index
Date: 2018/07/23
rating: 3
featured_image: images/tbgs/philosophy/art+philosophy5.jpg
rating: 4
save_as: TBGS_index.html
Tags: galleries, schools, breadfellows,

# Art and Philosophy workshops and Summer School

I developed Art and Philosophy Workshops responding to exhibitions of Hannah Fitz and Otobong Nkanga and the Summer Schools in TempleBar Gallery and Studios in 2018 and 2019, in collaboration with Learning + Public Engagement Curators Katy Fitzpatrick, Roisin Bohane and Professor of Philosophy of Education Dr Aislinn O’Donnell.

These workshops employed a variety of materials and considered form in sculpture and drawing, philosophical questioning and collaborative practice.
