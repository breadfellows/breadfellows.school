Title: Summer School
featured_image: images/tbgs/summer_school/tbgsss.jpeg
rating: 4
Date: 2018/06/23
rating: 3

# Making Connections Summer School at Templebar Gallery and Studios in collaboration with Learning + Public Engagement Curator Katy Fitzpatrick (2018) and Roisín Bohane (2019).

These workshops were developed as part of a Summer School at Templebar Gallery and Studios, over 4 days, the entire gallery space at TBG+S was dedicated to a range of drop-in activities, art workshops, and a pop up cafe. These activities are free and open for all attend.
I facilitated conversations about collaborative practice and Breadfellows' Chats with children and adults visiting the galleries.

![](/images/tbgs/summer_school/tbgsss.jpeg)
![](/images/tbgs/summer_school/tbgsss2.jpeg)
![](/images/tbgs/summer_school/tbgsss3.jpeg)